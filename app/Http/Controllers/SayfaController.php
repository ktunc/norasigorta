<?php

namespace App\Http\Controllers;

use Mailgun\Mailgun;
use PHPMailer\PHPMailer\PHPMailer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Haber;
use App\Model\HaberMail;

class SayfaController extends Controller
{
    public function index(){
        $haberler = Haber::where(['silindi'=>0, 'aktif'=>1])->orderBy('created_at','desc')->limit(6)->get();
        return view('index',['haberler'=>$haberler]);
    }

    public function haber(Request $request){
        $haber = Haber::where(['silindi'=>0, 'aktif'=>1, 'id'=>$request->haberid])->first();
        return view('haber',['haber'=>$haber]);
    }

    public function habermailkayit(Request $request){
        $return = array('hata'=>true, 'mesaj'=>'Bir hata medyana geldi. Lütfen tekrar deneyin.');
        if($request->isMethod('POST')){
            $habermail = HaberMail::where(['mail'=>$request->habermail])->first();

            if($habermail){
                $return['mesaj'] = 'Mail adresiniz zaten kayıtlı.';
            }else{
                $habermail = new HaberMail();
                $habermail->mail = $request->habermail;
                $habermaildurum = $habermail->save();
                if($habermaildurum){
                    $return['hata'] = false;
                }
            }
        }

        return response()->json($return);
    }

    public function teklifal(Request $request){
        $return = array('hata'=>true, 'mesaj'=>'Bir hata medyana geldi. Lütfen tekrar deneyin.');
        if($request->isMethod('POST')){
            $mgClient = Mailgun::create(env('MAILGUN_SECRET'), 'https://api.eu.mailgun.net');

            if($request->teklifturu == 'trafik'){
                $veri = array(
                    'title'=>'Trafik Sigortası Teklif Talebi',
                    'sehir'=>$request->il,
                    'telefon'=>$request->tel,
                    'mail'=>$request->mail,
                    'marka'=>$request->marka,
                    'model'=>$request->model,
                    'model_yili'=>$request->modelyili,
                    'plaka'=>$request->plaka,
                    'template'=>'trafik_teklifi'
                );

                if($request->ruhsat == 'sahis'){
                    $veri['ruhsat_sahibi'] = 'Şahıs';
                    $veri['kimlik_title'] = 'T.C. Kimlik No';
                    $veri['kimlik_no'] = $request->tc;
                    $veri['isim_title'] = 'Ad Soyad';
                    $veri['isim'] = $request->adsoyad;

                }else if($request->ruhsat == 'sirket'){
                    $veri['ruhsat_sahibi'] = 'Şirket';
                    $veri['kimlik_title'] = 'Vergi Kimlik No';
                    $veri['kimlik_no'] = $request->vergino;
                    $veri['isim_title'] = 'Şirket Adı';
                    $veri['isim'] = $request->sirketadi;
                }

                if($request->ruhsatislemturu == 'yenileme'){
                    $veri['islem'] = 'Yenileme';
                    $veri['seri_no_title'] = 'Belge Seri No';
                    $veri['seri_no'] = $request->belgeno;
                } else if($request->ruhsatislemturu == 'yenikayit'){
                    $veri['islem'] = 'Yeni Kayıt';
                    $veri['seri_no_title'] = 'ASBİS No';
                    $veri['seri_no'] = $request->asbis;
                }

            } else if($request->teklifturu == 'kasko'){
                $veri = array(
                    'title'=>'Kasko Teklif Talebi',
                    'sehir'=>$request->il,
                    'telefon'=>$request->tel,
                    'mail'=>$request->mail,
                    'marka'=>$request->marka,
                    'model'=>$request->model,
                    'model_yili'=>$request->modelyili,
                    'plaka'=>$request->plaka,
                    'template'=>'trafik_teklifi'
                );

                if($request->ruhsat == 'sahis'){
                    $veri['ruhsat_sahibi'] = 'Şahıs';
                    $veri['kimlik_title'] = 'T.C. Kimlik No';
                    $veri['kimlik_no'] = $request->tc;
                    $veri['isim_title'] = 'Ad Soyad';
                    $veri['isim'] = $request->adsoyad;

                }else if($request->ruhsat == 'sirket'){
                    $veri['ruhsat_sahibi'] = 'Şirket';
                    $veri['kimlik_title'] = 'Vergi Kimlik No';
                    $veri['kimlik_no'] = $request->vergino;
                    $veri['isim_title'] = 'Şirket Adı';
                    $veri['isim'] = $request->sirketadi;
                }

                if($request->ruhsatislemturu == 'yenileme'){
                    $veri['islem'] = 'Yenileme';
                    $veri['seri_no_title'] = 'Belge Seri No';
                    $veri['seri_no'] = $request->belgeno;
                } else if($request->ruhsatislemturu == 'yenikayit'){
                    $veri['islem'] = 'Yeni Kayıt';
                    $veri['seri_no_title'] = 'ASBİS No';
                    $veri['seri_no'] = $request->asbis;
                }

            } else if($request->teklifturu == 'saglik'){
                $veri = array(
                    'title'=>'Sağlık Sigortası Teklif Talebi',
                    'kimlik_no'=>$request->tc,
                    'isim'=>$request->adsoyad,
                    'dtarih'=>$request->dTarih,
                    'telefon'=>$request->tel,
                    'mail'=>$request->mail,
                    'meslek'=>$request->meslek,
                    'sehir'=>$request->il,
                    'teminat'=>$request->teminaticerigi,
                    'template'=>'saglik_teklifi'
                );
            } else if($request->teklifturu == 'konut'){
                $veri = array(
                    'title'=>'Konut Sigortası Teklif Talebi',
                    'isim'=>$request->adsoyad,
                    'telefon'=>$request->tel,
                    'mail'=>$request->mail,
                    'sehir'=>$request->il,
                    'mkare'=>$request->mkare,
                    'konut_bedeli'=>$request->konut_bedeli,
                    'adres'=>$request->adres,
                    'template'=>'konut_teklifi'
                );

            } else{
                return $return;
            }

            $donen = $mgClient->messages()->send(env('MAILGUN_DOMAIN'), array(
                'from'=>env('MAIL_FROM_ADDRESS'),
                'to'=>'mail@kaantunc.com',
                'h:Reply-To'=>$request->mail,
                'subject'=>$veri['title'],
                'template'=>$veri['template'],
                'h:X-Mailgun-Variables'    => json_encode($veri, JSON_UNESCAPED_UNICODE)
            ));

//            $dns = $mgClient->domains()->show(env('MAILGUN_DOMAIN'))->getInboundDNSRecords();
//            $return['dns'] = $dns;
            $return['hata'] = false;
        }

        return $return;
    }
}
