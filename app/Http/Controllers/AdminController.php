<?php

namespace App\Http\Controllers;

use App\Model\Haber;
use App\Model\HaberResim;
use SebastianBergmann\CodeCoverage\Report\Xml\File;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

use App\Plugins\FilerUploader;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function haberler(){
        $haberler = Haber::where(['silindi'=>0])->orderBy('created_at','desc')->paginate(10);
        return view('admin.haberler',['haberler'=>$haberler]);
//        $haberler = Haber::all();
//        return DataTables::of($haberler)->make(true);
    }

    public function haberedit(Request $request){
        $haber = Haber::find($request->haber_id);
        return view('admin.haberedit',['haber'=>$haber]);
    }

    public function haberresimsil(Request $request){
        if ($request->isMethod('post')) {
            $haberresim = HaberResim::find($request->haberresim);
            if($haberresim){
                Storage::disk()->delete($haberresim->path);
                $haberresim->delete();
                return response()->json(true);
            }else{
                return response()->json(false);
            }
        }else{
            return response()->json(false);
        }
    }

    public function haberkaydet(Request $request){
        if ($request->isMethod('post')) {
            if($request->haber_id != 0){
                $haber = Haber::find($request->haber_id);
                $haber->updated_at = date('Y-m-d H:i:s');
            } else{
                $haber = new Haber();
                $haber->created_at = date('Y-m-d H:i:s');
            }

            $haber->baslik = $request->baslik;
            $haber->icerik = $request->icerik;
            $haberdurum = $haber->save();
            
            if($haberdurum){
                if($request->has('yuklenenfile')){
                    $directory = 'haberler/'.$haber->id;
                    Storage::disk()->makeDirectory($directory);
                    foreach ($request->yuklenenfile as $row){
                        Storage::disk('public')->move('gecici/'.$row, $directory.'/'.$row);
                        $haberresim = new HaberResim();
                        $haberresim->haber_id = $haber->id;
                        $haberresim->path = $directory.'/'.$row;
                        $haberresim->save();
                    }
                }

                return response()->json(true);
            }else{
                return response()->json(false);
            }
        }else{
            return response()->json(false);
        }
    }

    public function uploadimage(Request $request){
        $uploader = new FilerUploader();

        Storage::makeDirectory('test');

        $data = $uploader->upload($_FILES['files'], array(
            'limit' => 10, //Maximum Limit of files. {null, Number}
            'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => public_path() . '/storage/gecici/', //Upload directory {String}
            'title' => array('randomVEtime'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => false, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));

        if($data['isComplete']){
            $files = $data['data'];
//            Storage::disk('public')->move('gecici/'.$files['metas'][0]['name'], 'haberler/'.$files['metas'][0]['name']);
//            \File::copy(public_path() . '/uploads/gecici/'.$files['metas'][0]['name'], public_path() . '/uploads/haberler/'.$files['metas'][0]['name']);
            return response()->json($files['metas'][0]['name']);
        }

        if($data['hasErrors']){
            $errors = $data['errors'];
            return response()->json($errors);
        }

    }

    public function removeimage(Request $request){
        Storage::disk('public')->delete('gecici/'.$request->file);
        return response()->json($request->file);
    }

    public function ajaxhaberaktifpasif(Request $request){
        if ($request->isMethod('post')) {
            if($request->has('haber') && $request->has('aktifpasif')){
                $haber = Haber::find($request->haber);
                $haber->aktif = $request->aktifpasif;
                $haber->save();
                return response()->json(true);
            }else{
                return response()->json(false);
            }
        }else{
            return response()->json(false);
        }
    }

    public function ajaxhabersil(Request $request){
        if ($request->isMethod('post')) {
            if($request->has('haber')){
                $haber = Haber::find($request->haber);
                $haber->silindi = 1;
                $haber->save();
                return response()->json(true);
            }else{
                return response()->json(false);
            }
        }else{
            return response()->json(false);
        }
    }

    public function ajaxhaberler(){
        return datatables(Haber::all('id', 'baslik', 'created_at'))->toJson();
    }
}
