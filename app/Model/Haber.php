<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Haber extends Model
{
    protected $table = 'haberler';

    public function haberresimleri()
    {
        return $this->hasMany('App\Model\HaberResim');
    }
}
