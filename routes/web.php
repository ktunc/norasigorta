<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Storage;

Route::get('/', 'SayfaController@index')->name('index');

Route::get('/index', 'SayfaController@index')->name('index');
Route::get('/haber/{haberid}', 'SayfaController@haber')->name('haber');

Route::post('/habermailkayit', 'SayfaController@habermailkayit');
Route::post('/teklifal', 'SayfaController@teklifal');

Route::get('/iletisim', function () {
    return view('iletisim');
});

Route::get('/hakkimizda', function () {
    return view('hakkimizda');
});

Route::get('/misyonvizyon', function () {
    return view('misyonvizyon');
});

Route::get('/degerlerimiz', function () {
    return view('degerlerimiz');
});

Route::get('/bireysel_emeklilik', function () {
    return view('urunler/bireysel_emeklilik');
});

Route::get('/ferdi_kaza', function () {
    return view('urunler/ferdi_kaza');
});

Route::get('/isyeri', function () {
    return view('urunler/isyeri');
});

Route::get('/hekim_sorumluluk', function () {
    return view('urunler/hekim_sorumluluk');
});

Route::get('/kasko', function () {
    return view('urunler/kasko');
});

Route::get('/muhendislik', function () {
    return view('urunler/muhendislik');
});

Route::get('/nakliyat', function () {
    return view('urunler/nakliyat');
});

Route::get('/saglik', function () {
    return view('urunler/saglik');
});

Route::get('/seyahat', function () {
    return view('urunler/seyahat');
});

Route::get('/sorumluluk', function () {
    return view('urunler/sorumluluk');
});

Route::get('/tarim', function () {
    return view('urunler/tarim');
});

Route::get('/trafik', function () {
    return view('urunler/trafik');
});

Route::get('/yangin', function () {
    return view('urunler/yangin');
});

Route::get('/yat_tekne', function () {
    return view('urunler/yat_tekne');
});

Route::get('/konut', function () {
    return view('urunler/konut');
});

Route::get('/hayat', function () {
    return view('urunler/hayat');
});

Route::get('/hizmet_hasar', function () {
    return view('hizmetler/hizmet_hasar');
});

Route::get('/hizmet_arac_deger', function () {
    return view('hizmetler/hizmet_arac_deger');
});

Route::get('/hizmet_kurumsal_risk', function () {
    return view('hizmetler/hizmet_kurumsal_risk');
});

Route::get('/hizmet_sigorta_danismanlik', function () {
    return view('hizmetler/hizmet_sigorta_danismanlik');
});

Route::get('/hizmet_ticari_alacak', function () {
    return view('hizmetler/hizmet_ticari_alacak');
});



Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/admin', function () {
//    return view('admin/index');
//})->middleware('auth');

//Route::get('/admin', function () {
//    return view('admin/index');
//})->middleware('auth');

//Route::get('/admin/{page?}', function ($page='') {
//    return view('admin/'.$page);
//})->middleware('auth');

Route::group(['middleware' => 'auth'], function (){
    Route::post('/admin/uploadimage', 'AdminController@uploadimage');
    Route::post('/admin/removeimage', 'AdminController@removeimage');
    Route::post('/admin/haberkaydet', 'AdminController@haberkaydet');
    Route::post('/admin/haberresimsil', 'AdminController@haberresimsil');
    Route::post('/admin/ajaxhaberler', 'AdminController@ajaxhaberler');
    Route::post('/admin/ajaxhabersil', 'AdminController@ajaxhabersil');
    Route::post('/admin/ajaxhaberaktifpasif', 'AdminController@ajaxhaberaktifpasif');
    Route::get('/admin/haberler', 'AdminController@haberler');

    Route::get('/admin', function () {
        return view('admin/index');
    });

    Route::get('/admin/index', function () {
        return view('admin/index');
    });

    Route::get('/admin/yenihaber', function () {
        return view('admin/yenihaber');
    });

    Route::get('/admin/haberedit/{haber_id}', 'AdminController@haberedit');
});

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);