@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Değerlerimiz</h4>
                        <p>İnsan odaklı, saygıyı esas alan, hasar anında uygun çözümler sunarak her zaman memnuniyeti üst düzeyde tutan bir yönetim anlayışı göstermek.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection