@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Araç Değer Kaybı</h4>
                        <p>Araç değer kaybı, kaza sebebiyle onarım gören araçların ikinci el piyasa değerlerinde meydana gelen maddi kaybı ifade eder.</p>
                        <p>Aracınız kazada herhangi bir hasar görmüşse tamamen ve en düzgün şekilde tamir edilmiş olsa dahi aracınızda değer kaybı oluşur. Bu konuda dikkat edilmesi gereken husus hasar gören bölgenin hiç onarım görmemiş ya da benzer nitelikte bir onarım görmemiş olmasıdır.</p>
                        <p>Örnek olarak; daha önce başka bir kaza sebebiyle boyanan bir parçanın daha sonra başka bir kaza sebebiyle tekrar boyanması değer kaybına sebep olmazken; bu parçanın değiştirilmiş olması ülkemiz koşullarında araç değer kaybına sebep olacaktır.</p>

                        <h4>Araç Değer Kaybı Nasıl Hesaplanır?</h4>
                        <p>Araç değer kaybı hesaplamasında aracın;</p>
                        <ul>
                            <li>Hasar geçmişi ve niteliği</li>
                            <li>Kilometresi</li>
                            <li>Marka ve model bilgisi</li>
                            <li>Üretim yılı</li>
                            <li>Pazar değeri</li>
                        </ul>
                        <p>ve bunlar gibi bir çok faktör dikkate alınarak değerleme yapılır.</p>

                        <h4>Araç Değer Kaybı Tespiti Kime Yaptırılır?</h4>
                        <p>Hazine Müsteşarlığı Sigortacılık Genel Müdürlüğü’nce 23.09.2010 tarihinde yayınlanan sektör duyurusu ile araç değer kaybı tespitinin kara araçları branşında ruhsat sahibi sigorta eksperlerince yapılabileceği belirtilmiştir.</p>

                        <h4>Araç Değer Kaybı Hakkında Bilinmesi Gerekenler</h4>
                        <p>Birçok sigorta firması araç tamponları ve diğer plastik akşamların araçta  değer kaybına sebep olmayacağı  gerekçesiyle reddetme eğilimindedir; ancak bu genel geçer bir kural değildir.</p>
                        <p>Örnek olarak son model bir arabayı ele aldığımızda bu aracın hasarsız hali ile ön ve arka tamponu boyalı halinin değeri eşittir diyemeyiz. Ayrıca birçok sigorta şirketi kaza geçiren aracın daha önce de kazası bulunduğu iddiasıyla değer kaybı taleplerini reddetmektedir. Bu tip durumlarda karşılaştığınızda sigorta eksperleri tarafından hazırlanan değer kaybı raporu ile sigorta şirketlerinden tazminat talep edebilir ya da hukuki süreci başlatabilirsiniz.</p>

                        <h4>Araç Değer Kaybında Zaman Aşımı</h4>
                        <p>Araç Değer kaybı davalarında zaman aşımı süresi 2 yıldır. İki yıl içinde kaza yapmışsanız değer kaybı talebinde bulunabilirsiniz.</p>
                        <p>Detaylı bilgi için bizi arayın!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection