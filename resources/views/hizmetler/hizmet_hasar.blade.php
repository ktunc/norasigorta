@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Hasar Anında</h4>
                        <p>Hasar anında hiç vakit kaybetmeden acenteliğimiz ile <a class="text-nora" href="{{url('/iletisim')}}"><b>iletişime</b></a> geçmenizi tavsiye ederiz.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection