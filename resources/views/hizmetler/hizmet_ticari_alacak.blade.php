@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Ticari Alacak Sigortası Nedir?</h4>
                        <p>Alacak sigortası, işinizi ticari borçların ödenmemesi riskine karşı korur. Faturalarınızın ödenmesini temin eder, ticari faaliyetlerin getirdiği kurumsal ve siyasi riskleri güvenilir bir şekilde yönetmenizi sağlar.</p>
                        <p><b>Ticari bir alacağın ödenmemesine</b> bağlı olarak ortaya çıkabilecek kayıpları tazmin eden alacak sigortasının nihai amacı, faaliyetlerinizi büyük ölçekli kayıplardan korumak ve kârlı bir şekilde büyümesine yardımcı olmaktır. Temel olan, şirketler, sektör ve ekonomik trendler hakkında güncel veriye sahip olarak bilgiye dayalı kredi kararları almak, kayıpların önüne geçmek ve onları asgariye indirmektir.</p>

                        <h4>Ödenemeyen Faturalara Dair Yetersiz Bilgi</h4>
                        <p><b>Ödenmeyen faturalar</b> bir şirketin varlıklarının yüzde 35’i kadarını oluşturabilmekte, tahsil edilememeleri durumunda ise büyük zararlara neden olmaktadır. Müşterilerinizin ödeme gücüne dair yetersiz bilgi, ödenmeyen faturaların temel nedenidir.</p>
                        <p>Bilgi birikimimiz, doğru müşteriyi, doğru pazarları ve kredi limitlerini tayin etmenize yardımcı olmakta; ticari alacakların ödenmemesi riskini ortadan kaldırarak asgariye indirmektedir. Sonuç itibarı ile, mevcut müşterilerinize daha çok kredi limiti tanımlayabilecek ve normal koşullarda daha riskli algılayacağınız yeni ve daha büyük müşterilere erişmeyi hedefleyen çalışmalarınızda çok daha güvenli bir şekilde hareket edebileceksiniz.</p>
                        <p>Müşterileriniz ödeme yapmazsa, sigortalanmış faturaların bedelini size ödeyerek  borç tahsilatı ile biz ilgileneceğiz.</p>
                        <p>Açık hesap çalışan herhangi bir şirket alacak sigortasından faydalanabilmektedir. Müşterilerimiz arasında her sektörden ve ölçekten şirket bulunmaktadır. Aralarında hizmet ve mal ticaretinin de yer aldığı çok sayıda farklı sektörden firma, işlerinin başarısında alacak sigortasından yararlanmaktadır.</p>

                        <h4>Alacak Sigortası Örneği</h4>
                        <p>Bir şirketin kâr marjının %5 olduğu bir senaryo düşünelim. Şirket <b>müşterilerinden birinin 100.000 Dolar</b> tutarındaki bir ticari borcunda temerrüde düştüğünü varsayalım. Böylesi bir durumda ortaya çıkacak kâr kaybını telafi etmesi için <b>2.000.000 Dolar</b> tutarında ilave satış gerçekleştirmesi gerekecektir.</p>
                        <p>Ticari alacak sigorta poliçesi, alacak hesaplarınızı yönetmenize yardımcı olur ve ödeme olmaması halinde sizi tazmin eder. En önemlisi, ödenmeme durumunda nakit akışında karşılaşacağınız kayıp sizi çok zor durumda bırakabilir. Ödeme yapılmaması şirketinizi zayıflatır ve yatırım kapasitenizi de düşürür.</p>
                        <p><b>Ticari Alacak Sigortasının İşleyiş Şekli ve Daha Detaylı Bilgi İçin Bizimle İletişime Geçin.</b></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection