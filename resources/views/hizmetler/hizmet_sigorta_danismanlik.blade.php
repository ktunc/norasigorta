@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Risk Yönetimi</h4>
                        <p>Temel anlamda risk yönetimi, yapının aniden karşılaştığı olağanüstü durumlarda sergileyeceği planlı ve işleri yoluna koyucu bir reaksiyonlar bütünü olarak ifade edilebilir. Kriz anında şirketin atacağı adımların daha önceden planlanması bunun bir göstergesi olabilir.</p>

                        <h4>Risk Analizi</h4>
                        <p>Bugün dünyanın en gelişmiş şirketlerinde ve yapılarında, mümkün mertebe en ayrıntılı ve en önemli risk analizi çalışmaları hayat bulur. Çünkü hem yapının geleceği, hem de kazanç ve ayakta kalma isteği bu analizin ciddi şekilde yapılmasını ve uygulanmasını gerekli kılar.</p>

                        <h4>Risk Analizi Nasıl Yapılır?</h4>
                        <p>Tüm bu tanımların ardından, elbette ki <b>risk analizi nasıl yapılır</b> sorusu ortaya çıkacaktır. Risk analizi yapılırken yapının mevcut durumu tüm detayları ile incelenir. Bu mevcut durum içinde karşı karşıya kalınması muhtemel tehlikeler tespit edilir. Bu durum mali yapıda olabilir ya da fiziksel durumlar da risk analizinde etkendir. Yani risk analizi sektöre ve çalışma alanına göre değişkenlik gösterebilir. Ancak ne olursa olsun, tüm tehlikeler ve riskler kağıt üzerine aktarılmalı ve gereken tedbirler bu risklere göre alınmalıdır.</p>

                        <h4>Risk Yönetimi Gerekli Midir?</h4>
                        <p>Analiz yapılmasının ardından <b>risk yönetimi nedir</b> sorusu da gündeme gelebilir. Çünkü risk analizi ile beraber bu ortamın yönlendirilmesi ve yönetilmesi de önemli ihtiyaçlardır. İlk olarak risklerin ortadan kaldırılması, bunun için çalışmalar yapılması şarttır. Bunun yanı sıra, ortadan kaldırılamayan ya da yok edilemeyen risklere ikame yöntemi uygulanmalıdır. İkame, daha az riski olan bir durumu ya da malzemeyi daha riskli olanın yerine koymaktır. Bu şekilde tehlikeler ve riskler önlenemese de olasılıkları ve şiddetleri düşürülür.</p>

                        <p>Doğru teminatlara sahip, işletmenizi bütün önlemlere rağmen olası risklere karşı koruyan bir sigorta poliçesine sahip olmak istiyorsanız, bunun yolu Risk Yönetiminizden geçer. Sizinle beraber risklerinizi analiz etmeye hazırız.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection