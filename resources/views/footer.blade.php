<!-- Footer -->
<footer class="footer">

    <!-- Footer Contact Area -->
    <div class="footer-contact-area">
        <div class="container">
            <div class="footer-contact">
                <div class="row">
                    <div class="col">
                        <div class="footer-contact-block">
									<span class="footer-contact-icon">
										<i class="zmdi zmdi-phone"></i>
									</span>
                            <p><a href="#">+90(312)3940323</a><br><a href="#">+90(544)9708102</a></p>
                        </div>
                    </div>
                    <div class="col">
                        <div class="footer-contact-block">
									<span class="footer-contact-icon">
										<i class="zmdi zmdi-home"></i>
									</span>
                            <p>Melih Gökçek Bulvarı İvedik İş Merkezi No:8/21 İvedik OSB / ANKARA</p>
                        </div>
                    </div>
                    <div class="col">
                        <div class="footer-contact-block">
									<span class="footer-contact-icon">
										<i class="zmdi zmdi-email"></i>
									</span>
                            <p><a href="#">leventmurat@norasigorta.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// Footer Contact Area -->

    <!-- Footer Inner -->
    <div class="footer-inner">

        <!-- Footer Widgets Area -->
        <div class="footer-widgets-area section-padding-lg">
            <div class="container">
                <div class="row widgets footer-widgets">

                    <div class="col-lg-12 text-center">
                        <div class="single-widget widget-info">
                            <div class="logo">
                                <a href="index.html">
                                    <img src="{{ asset('images/logo/logo.png') }}" alt="footer logo">
                                </a>
                            </div>
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
                                {{--incisequi nesciunt. Neque--}}
                                {{--porro quisquam.</p>--}}
                            <ul class="footer-socialicons">
                                <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    {{--<div class="col-lg-2 col-md-6">--}}
                        {{--<div class="single-widget widget-links">--}}
                            {{--<h4 class="widget-title">--}}
                                {{--<span>Policy</span>--}}
                            {{--</h4>--}}
                            {{--<ul>--}}
                                {{--<li><a href="#">Term</a></li>--}}
                                {{--<li><a href="#">Licenses</a></li>--}}
                                {{--<li><a href="#">Fund</a></li>--}}
                                {{--<li><a href="#">Support</a></li>--}}
                                {{--<li><a href="#">Security</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-lg-3 col-md-6">--}}
                        {{--<div class="single-widget widget-latestblog">--}}
                            {{--<h4 class="widget-title">--}}
                                {{--<span>Latest Blog</span>--}}
                            {{--</h4>--}}
                            {{--<ul>--}}
                                {{--<li>--}}
                                    {{--<div class="widget-latestblog-image">--}}
                                        {{--<a href="blog-details.html">--}}
                                            {{--<img src="{{ asset('images/blog/thumbnails/blog-thumbnail-1.png') }}" alt="blog thumbnail">--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    {{--<span>1st Janu, 2018</span>--}}
                                    {{--<h5><a href="blog-details.html">Ipsam rerum nisi beatae et</a></h5>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<div class="widget-latestblog-image">--}}
                                        {{--<a href="blog-details.html">--}}
                                            {{--<img src="{{ asset('images/blog/thumbnails/blog-thumbnail-2.png') }}" alt="blog thumbnail">--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    {{--<span>1st Janu, 2018</span>--}}
                                    {{--<h5><a href="blog-details.html">Ipsam rerum nisi beatae et</a></h5>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-lg-6 col-md-6">--}}
                        {{--<div class="single-widget widget-newsletter">--}}
                            {{--<h4 class="widget-title">--}}
                                {{--<span>Bizden Haber Alın</span>--}}
                            {{--</h4>--}}
                            {{--<p>Yeni sigorta tekliflerinden haberdar olmak için üye olun.</p>--}}
                            {{--<form action="#" id="haberalinkaydol" class="widget-newsletter-form">--}}
                                {{--<input type="email" name="habermail" placeholder="Mail Adresiniz" required>--}}
                                {{--<button type="submit"><img src="{{ asset('images/icons/paper-plane-white.png') }}" alt="send"></button>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                </div>
            </div>
        </div>
        <!--// Footer Widgets Area -->

        <!-- Footer Copyright Area -->
        <div class="footer-copyright-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12 col-12 text-center">
                        <p class="copyright-text">Copyright {{date('Y')}} &copy; </p>
                    </div>
                    {{--<div class="col-lg-6 col-12">--}}
                        {{--<ul class="copyright-links">--}}
                            {{--<li><a href="#">Help</a></li>--}}
                            {{--<li><a href="#">About</a></li>--}}
                            {{--<li><a href="#">Services</a></li>--}}
                            {{--<li><a href="#">Privacy</a></li>--}}
                            {{--<li><a href="#">Support </a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
        <!--// Footer Copyright Area -->

    </div>
    <!--// Footer Inner -->

</footer>
<!--// Footer -->

<script type="text/javascript">
$(document).ready(function () {
    $('form#haberalinkaydol').submit(function (event) {
        event.preventDefault();
        $.ajax({
            type:'POST',
            url:'{{url('/habermailkayit')}}',
            data:{'habermail':$('input[name="habermail"]').val()},
            dataType:'json'
        }).done(function (data) {
            if(data.hata){
                $.alert({
                    columnClass: 'm',
                    theme:'modern',
                    type:'red',
                    icon:'fa fa-close',
                    title:'Hata',
                    content:data.mesaj
                });
            }else{
                $.alert({
                    columnClass: 'm',
                    theme:'modern',
                    type:'green',
                    icon:'fa fa-check',
                    title:'Başarılı',
                    content:'Mail adresiniz başarıyla kaydedilmiştir.'
                });
            }
        }).fail(function () {
            $.alert({
                columnClass: 'm',
                theme:'modern',
                type:'red',
                icon:'fa fa-close',
                title:'Hata',
                content:'Bir hata meydana geldi. Lütfen tekrar deneyin.'
            });
        });
    });
});
</script>