<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="{{asset('images/logo/logo.png')}}" width="100%"/>
                             </span>
                    {{--<a data-toggle="dropdown" class="dropdown-toggle" href="#">--}}
                            {{--<span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David Williams</strong>--}}
                             {{--</span> <span class="text-muted text-xs block">Art Director <b class="caret"></b></span> </span> </a>--}}
                    {{--<ul class="dropdown-menu animated fadeInRight m-t-xs">--}}
                        {{--<li><a href="profile.html">Profile</a></li>--}}
                        {{--<li><a href="contacts.html">Contacts</a></li>--}}
                        {{--<li><a href="mailbox.html">Mailbox</a></li>--}}
                        {{--<li class="divider"></li>--}}
                        {{--<li><a href="login.html">Logout</a></li>--}}
                    {{--</ul>--}}
                </div>
                {{--<div class="logo-element">--}}
                    {{--IN+--}}
                {{--</div>--}}
            </li>
            <li class="active">
                <a href="javascript:void(0)"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Haberler</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{{url('/admin/haberler')}}">Tüm Haberler</a></li>
                    <li><a href="{{url('/admin/yenihaber')}}">Yeni Haber Ekle</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i> Çıkış
                </a>
            </li>

        </ul>

    </div>
</nav>