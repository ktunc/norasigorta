@extends('admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Haberler</h5>
                    <div class="ibox-tools">
                        {{--<a class="collapse-link">--}}
                            {{--<i class="fa fa-chevron-up"></i>--}}
                        {{--</a>--}}
                        {{--<a class="dropdown-toggle" data-toggle="dropdown" href="#">--}}
                            {{--<i class="fa fa-wrench"></i>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu dropdown-user">--}}
                            {{--<li><a href="#">Config option 1</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#">Config option 2</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<a class="close-link">--}}
                            {{--<i class="fa fa-times"></i>--}}
                        {{--</a>--}}
                        <a href="{{url('/admin/yenihaber')}}" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i> Yeni Haber Ekle</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-hover table-bordered" id="haberTable">
                                <thead class="thead-dark">
                                <tr>
                                    <th width="5%">ID</th>
                                    <th>Başlık</th>
                                    <th width="10%">Aktif</th>
                                    <th width="10%">Tarih</th>
                                    <th width="10%">İşlem</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($haberler as $haber)
                                    <tr>
                                        <td class="text-center">{{$haber->id}}</td>
                                        <td>{{$haber->baslik}}</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="haberaktif" {{$haber->aktif==1?'checked="checked"':''}} value="{{$haber->id}}" />
                                            <label for="haberaktif">{{$haber->aktif==1?'Aktif':'Pasif'}}</label>
                                        </td>
                                        <td class="text-center">{{$haber->created_at}}</td>
                                        <td class="text-center">
                                            <a href="{{url('/admin/haberedit/'.$haber->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                                            <button type="button" class="btn btn-danger btn-sm" onclick="FuncHaberSil({{$haber->id}})"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-12 text-center">
                            {{ $haberler->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--{!! $dataTable->scripts() !!}--}}

    <script type="text/javascript">
        $(document).ready(function(){
            $('input#haberaktif').each(function(){
                var self = $(this),
                    label = self.next(),
                    label_text = label.text();

                label.remove();
                self.iCheck({
                    checkboxClass: 'icheckbox_line-red',
                    checkedCheckboxClass:'checked icheckbox_line-green',
                    insert: '<div class="icheck_line-icon"></div><span class="labeltext">' + label_text +'</span>'
                });
            });

            $('input#haberaktif').on('ifChecked',function () {
                $(this).closest('div').find('.labeltext').html('Aktif');
                FuncHaberAktifPasif($(this).val(), 1);
            });

            $('input#haberaktif').on('ifUnchecked',function () {
                $(this).closest('div').find('.labeltext').html('Pasif');
                FuncHaberAktifPasif($(this).val(), 0);
            });
        });

        function FuncHaberAktifPasif(haber, aktifpasif) {
            $.ajax({
                type:'POST',
                url:'{{url('/admin/ajaxhaberaktifpasif')}}',
                data:{'haber':haber, 'aktifpasif':aktifpasif}
            }).done(function (data) {

            }).fail(function () {
                $.alert({
                    columnClass:'m',
                    theme:'modern',
                    type:'red',
                    icon:'fa fa-close',
                    title:'Hata',
                    content:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                });
            });
        }

        function FuncHaberSil(haber) {
            $.confirm({
                columnClass:'m',
                theme:'modern',
                type:'orange',
                icon:'fa fa-exclamation',
                title:'Haberi Silmek İstediğinizden Emin Misiniz?',
                content:'',
                buttons:{
                    vazgec:{
                        text:'Vazgeç'
                    },
                    sil:{
                        text:'SİL',
                        btnClass:'btn-danger',
                        action:function () {
                            $.ajax({
                                type:'POST',
                                url:'{{url('/admin/ajaxhabersil')}}',
                                data:{'haber':haber},
                                beforeSend:function () {
                                    $.blockUI();
                                }
                            }).done(function (data) {
                                if(data){
                                    $.alert({
                                        columnClass:'m',
                                        theme:'modern',
                                        type:'green',
                                        icon:'fa fa-check',
                                        title:'Başarılı',
                                        content:'Haber başarıyla silindi.',
                                        onClose:function () {
                                            window.location.reload();
                                        }
                                    });
                                }else{
                                    $.alert({
                                        columnClass:'m',
                                        theme:'modern',
                                        type:'red',
                                        icon:'fa fa-close',
                                        title:'Hata',
                                        content:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                                        onContentReady:function () {
                                            $.unblockUI();
                                        }
                                    });
                                }
                            }).fail(function () {
                                $.alert({
                                    columnClass:'m',
                                    theme:'modern',
                                    type:'red',
                                    icon:'fa fa-close',
                                    title:'Hata',
                                    content:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                                    onContentReady:function () {
                                        $.unblockUI();
                                    }
                                });
                            });
                        }
                    }
                }
            });
        }
        // $(document).ready(function () {
        //     $('#haberTable').DataTable();
        // });
    </script>
@endsection
