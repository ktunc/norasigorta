@extends('admin')

@section('content')
    <link href="{{ asset('css/admin/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-filer/jquery.filer.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-filer/jquery.filer-dragdropbox-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-filer/jquery.filer-dragdropbox-theme.css') }}" rel="stylesheet">

    <script src="{{ asset('js/admin/plugins/summernote/summernote.min.js') }}"></script>
    <script src="{{ asset('js/jquery-filer/jquery.filer.min.js') }}"></script>


    <div class="row">
        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Yeni Haber</h5>
                    <div class="ibox-tools">
                        {{--<a class="collapse-link">--}}
                        {{--<i class="fa fa-chevron-up"></i>--}}
                        {{--</a>--}}
                        {{--<a class="dropdown-toggle" data-toggle="dropdown" href="#">--}}
                        {{--<i class="fa fa-wrench"></i>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu dropdown-user">--}}
                        {{--<li><a href="#">Config option 1</a>--}}
                        {{--</li>--}}
                        {{--<li><a href="#">Config option 2</a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                        {{--<a class="close-link">--}}
                        {{--<i class="fa fa-times"></i>--}}
                        {{--</a>--}}
                        {{--<a href="{{url('/admin/yenihaber')}}" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i> Yeni Haber Ekle</a>--}}
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="FormHaber">
                        <input type="hidden" name="haber_id" value="0"/>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Başlık:</label>
                            <div class="col-lg-10"><input type="text" placeholder="Başlık" name="baslik" class="form-control" required></div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">İçerik:</label>
                            <div class="col-lg-10"><textarea placeholder="İçerik" name="icerik" class="form-control" required></textarea></div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Resim Yükle:</label>
                            <div class="col-lg-10">
                                <input type="file" name="files[]" id="filer_input2" multiple="multiple" accept="image/*"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-sm btn-primary" type="button" id="haberkaydet">Kaydet</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){

            $('textarea[name="icerik"]').summernote({
                height:'200px',
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert',['link','picture','video','table']]
                ]
            });

            $('form#FormHaber #haberkaydet').on('click',function () {
                if($('form#FormHaber input[name="baslik"]').val() == ''){

                }else if($('form#FormHaber input[name="baslik"]').val() == ''){

                }else{
                    var formdata = new FormData($('form#FormHaber').get(0));
                    $.ajax({
                        type:'POST',
                        url:"{{url('/admin/haberkaydet')}}",
                        data:formdata,
                        dataType:'json',
                        processData: false,
                        contentType: false,
                        cache:false,
                        beforeSend:function () {
                            $.blockUI();
                        }
                    }).done(function (data) {
                        if(data){
                            $.alert({
                                columnClass: 'm',
                                theme:'modern',
                                type:'green',
                                icon:'fa fa-check',
                                title:'Haber Başarılıyla Kaydedildi',
                                content:'',
                                onClose:function () {
                                    window.location.href = "{{url('/admin/haberler')}}";
                                }
                            });
                        }else{
                            $.alert({
                                columnClass: 'm',
                                theme:'modern',
                                type:'red',
                                icon:'fa fa-close',
                                title:'Hata',
                                content:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                                onOpenBefore:function () {
                                    $.unblockUI();
                                }
                            });
                        }
                    }).fail(function () {
                        $.alert({
                            columnClass: 'm',
                            theme:'modern',
                            type:'red',
                            icon:'fa fa-close',
                            title:'Hata',
                            content:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                            onOpenBefore:function () {
                                $.unblockUI();
                            }
                        });
                    });
                }
            });
        });
    </script>
    <script src="{{ asset('js/jquery-filer/jquery.filer.custom.js') }}"></script>
@endsection
