@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Misyonumuz</h4>
                        <p>Güvenli ve daha huzurlu bir gelecek için Sigorta bilincini ve farkındalığını yaratarak, herkesin sigorta ihtiyacını gündeme getirerek değer yaratmak.</p>
                        <h4>Vizyonumuz</h4>
                        <p>Sürekli olarak fark ve değer yaratan bir şirket olmak ve Herkes İçin Sigorta’yı kolay, yalın ve erişilebilir hale getirmek.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection