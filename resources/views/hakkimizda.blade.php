@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Hakkımızda </h4>
                        <p>Nora Sigorta; 2013 yılında Aktif Yaşam Sigorta olarak kurulmuş ve 2015 yılında Levent Murat tarafından satın alınarak unvan değişikliği ile şuanda ki ünvanı olan Nora Grup Sigorta Aracılık Hizm.Ltd.Şti. olarak İvedik Organize Sanayi Bölgesinde faaliyetine devam etmektedir.</p>

                        <p>Levent Murat, 2002 yılından bu yana Sigorta Sektöründe yer almaktadır. Çeşitli acentelerde, Kurumsal Müşteriler Yöneticisi, Acente Müdürlüğü gibi görevler almıştır. Kocaeli Üniversitesi Sigortacılık ve Anadolu Üniversitesi İşletme mezunudur. Evli ve 1 kız babasıdır. Şirketin adını kızı Nora’dan esinlenerek değiştirmiştir.</p>

                        <p>Bireysel Emeklilik Aracısı, Tarım Sigortaları TARSİM yetki belgesi, Segem sertifikaları vardır.</p>

                            <p>Yangın, mühendislik, sorumluluk, nakliyat, sağlık ve oto branşlarında edindiği deneyimlerini müşterilerinin ihtiyaçlarına uygun çözümlerle sunmayı ilke edinerek, hasar anında müşterinin yanında olmayı görev edinmiştir.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection