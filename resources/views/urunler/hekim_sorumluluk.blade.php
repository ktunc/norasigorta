@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Hekim Mesleki Sorumluluk Sigortası Nedir ve Neden Yaptırılmalıdır?</h4>
                        <p>Daha önce ABD ve Almanya gibi gelişmiş ülkelerde uygulanan hekim mesleki sorumluluk sigortası, hekimlerin mesleki uygulamaları sırasında oluşabilecek hatalar sonucunda ödenecek tazminatları karşılamak için, sigorta şirketleri tarafından sunulmaktadır. Şu anda ülkemizde rastlayabildiğimiz mesleki hatalar sonucunda ödenecek yüksek tazminatlara karşı hekimlerin güvence altında olmaları için bu sigortayı yaptırmaları büyük avantaj sağlayacaktır. Zorunlu Hekim Mesleki Sorumluluk Sigortası, mesleki hizmetler sırasında hastanın bir kusur veya ihmal sebebi ile yaralanması, sağlık durumlarının daha da kötüye gitmesi veya hayatlarını kaybetmeleri durumunda oluşabilecek tazminatları karşılar.</p>

                        <h4>Sigorta Primleri Neye Göre Belirlenir?</h4>
                        <p>Hekim mesleki sorumluluk sigortası primleri, hekimlerin çalıştığı daldaki risk gruplarına göre belirlenir. Şu anda 4 adet risk grubu bulunuyor. Bu risk gruplarından primleri en ucuz olan ve en düşük riske sahip olan 1. grup, en fazla riske sahip olan ve en fazla prime sahip olan grup ise 4. gruptur. Örnek vermek gerekirse, Çevre Sağlığı dalı 1. Grupta iken, Çocuk Acil 4. grupta yer almaktadır. Hangi grupta bulunduğunuza bağlı olarak primler ortalama 130 TL - 800 TL arasında değişkenlik gösterebilir. Tabi ki hangi grupta olursanız olun, yatırdığınız prim miktarı çoğaldıkça, alacağınız teminat miktarı da yükselmekte. Ayrıca sigorta primlerinin ödemeleri peşin olarak yapılıyor.</p>

                        <h4>Hekim Mesleki Sorumluluk Sigortası Yaptırırken Dikkat Edilmesi Gereken Konular Nelerdir?</h4>
                        <p>Bu sigorta türü de tıpkı Dask gibi zorunlu bir sigorta biçimidir ve bu sigortayı yaptırmayanlar için para cezası bulunmaktadır. Eğer hekim olarak çalışıyorsanız vakit kaybetmeden bir an önce bu sigortayı yaptırmalısınız. Yatıracağınız primlerin, sahip olacağınız teminat bedeline etki edeceğini hesap ederek priminizi dikkatli yatırın. Formu doldururken sizin branşınızı en iyi tanımlayan bölümü seçiniz, çünkü branşlara göre risk grupları ve dolayısıyla ödenecek primler farklılık gösterebilmektedir.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection