@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Emtia Nakliyat Sigortaları</h4>
                        <p>Kara, hava, deniz ve demiryolu taşımaları esnasında meydana gelebilecek ve Nakliyat Emtia Sigorta Poliçesi ile teminat altına alınan riskler:</p>
                        <ul>
                            <li>Kazalar</li>
                            <li>Hırsızlık</li>
                            <li>Yükleme-boşaltma hasarları</li>
                            <li>Doğal afetler</li>
                        </ul>

                        <h4>Yurtiçi Taşıyıcı Mali Mesuliyet (Sorumluluk) Sigortaları</h4>
                        <p>Üçüncü şahıslara ait muhtelif emtianın yurtiçinde karayolu ile taşınması sırasında, poliçede belirtilen kapsam çerçevesinde meydana gelecek kazalar nedeniyle taşınan emtianın zarara uğraması sonucu taşıyıcı firmaya düşen yasal mali sorumluluğu kapsar.</p>

                        <h4>Tekne (Hull) Sigortaları</h4>
                        <p>Gemilerin</p>
                        <ul>
                            <li>kötü hava şartları,</li>
                            <li>fırtına,</li>
                            <li>karaya oturma,</li>
                            <li>çarpma ve çarpışma</li>
                        </ul>
                        <p>sonucu uğrayacağı zararları teminat altına alır.</p>

                        <h4>Yat Sigortaları</h4>
                        <ul>
                            <li>Denizde,</li>
                            <li>Karada,</li>
                            <li>Tamirde,</li>
                            <li>Çekek yerinde,</li>
                            <li>Depoda</li>
                        </ul>
                        <p>iken yatların gövdeleri, motor veya makineleri, servis botları, donanım ve ekipmanları deniz veya diğer rizikolara karşı teminat altına alınır.</p>
                        <p>Üçüncü şahıslara verilecek maddi ve bedeni zararlar da bu sigorta kapsamı altına alınabilmektedir.</p>

                        <h4>Gemi İnşaat Sigortaları</h4>
                        <ul>
                            <li>Gemi inşaatı,</li>
                            <li>Tamiri,</li>
                            <li>Tadili</li>
                        </ul>
                        <p>ile ilgili rizikoları teminat altına alır.</p>

                        <h4>Liman/Terminal İşletenleri Sorumluluğu</h4>
                        <p>Liman işletme faaliyeti ile ilgili olan ve limanda bulunan deniz taşıtları, kargo, ekipman vb. ile 3.şahıs mallarında yükleme, boşaltma, yanaşma sırasında oluşabilecek zararlar, limana emanet olarak bırakılmış deniz taşıtları nedeniyle 3.şahıslara veya mallarına gelebilecek zararlar ve bu zararlar ile ilgili dava masraflarını teminat altına alır.</p>

                        <h4>Gemi Tamircileri Hukuki Sorumluluk Sigortası</h4>
                        <p>Gemi tamircilerinin/onarımcılarının iskeledeki, kızaktaki gemiler ve kendi kontrolündeki 3.şahıslara ait emtea, araç vb. ile 3.şahısların kendilerine gelebilecek gemi tamirciliği faaliyetinden kaynaklanan sorumluluk hasarlarını teminat altına alır.</p>

                        <h4>Marina İşletmecileri Hukuki Sorumluluk Sigortası</h4>
                        <p>Marina/Çekek Yeri işleten Sigortalının sigortalı mahalde marine/çekek yeri işletme faaliyeti ile ilgili olarak 3.şahıslara karşı verebileceği zararlardan kaynaklanan hukuki sorumluluğunu teminat altına alır.</p>

                        <h4>Depo İşletenleri Hukuki Sorumluluk Sigortası</h4>
                        <p>Depoda bulunan 3.şahıslara ait malların, depo içerisinde istiflenmesi, boşaltılması ve yüklenmesi sırasında mallarda doğrudan fiziksel olarak meydana gelecek zararlar neticesinde depo işleticisine karşı ileri sürülen yasal sorumluluk taleplerini temin eder.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection