@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>İnşaat All Risk Sigortası</h4>
                        <p>İnşaat all risk sigortası, inşaatın ön depolama safhasından başlar. Tahahhüt edilmiş işlerin kabulüne kadar geçen dönemde, müteahhit'in karşı karşıya kalabileceği ani ve beklenmedik riskler sonucu gerçekleşebilecek kayıp ve hasarları karşılar.</p>
                        <p>Bu teminata ek olarak ek prim alınarak, teminata dahil edilebilecek riskleri, üçüncü şahıs mali mesuliyet , enkaz kaldırma, fazla mesai ve seri vasıtalar, grev, lokavt, kargaşalık, halk hareketleri, terör, bakım devresi teminatları olarak sıralamak mümkündür.</p>
                        <p>Bu sigorta ile verilen teminatların süresi proje süresi ile sınırlı olup projenin bitirilip teslim edilmesi ile son bulur.</p>
                        <p>Teminat verilen bedel yani sigorta bedeli inşaat-montaj projesinin ulaşacağı son değer veya proje bedelidir.</p>

                        <h4>Montaj All Risk Sigortası</h4>
                        <p>Montaj all risk sigortası, montajın ön depolama safhasından başlar. Tahahhüt edilmiş işlerin kabulüne kadar geçen dönemde, müteahhit'in karşı karşıya kalabileceği ani ve beklenmedik riskler sonucu gerçekleşebilecek kayıp ve hasarları karşılar.</p>
                        <p>Bu teminatın yanında ek prim alınarak kapsama dahil edilebilecek riskleri, montaj ekipman, makineleri ve şantiye tesisleri üçüncü şahıs mali mesuliyet enkaz kaldırma fazla mesai ve seri vasıtalar grev, lokavt, kargaşalık, halk hareketleri, terör tecrübe devresi (4 hafta üzeri) teminatları olarak sıralamak mümkündür.</p>
                        <p>Bu sigorta ile verilen teminatların süresi proje süresi ile sınırlı olup projenin bitirilip teslim edilmesi ile son bulur.</p>
                        <p>Teminat verilen bedel yani sigorta bedeli inşaat-montaj projesinin ulaşacağı son değer veya proje bedelidir.</p>

                        <h4>Makina Kırılması Sigortası</h4>
                        <p>Bu sigorta, makinelerin çalışırken veya aynı iş yerinde temizleme, yer değiştirme esnasında oluşabilecek ani ve beklenmedik her türlü sebepten dolayı oluşabilecek hasarların giderilmesi için gereken tamirat ve ikame masraflarını temin eder.</p>
                        <p>Ek prim alınarak, bu teminata dahil edilebilecek riskleri, fiziki infilak temel ve kaideler fazla mesai ve seri vasıtalar grev, lokavt, kargaşalık, halk hareketleri, terör (yangın ve doğal afetler ana teminata dahil değildir.) olarak sıralamak mümkündür.</p>
                        <p>Sigorta koruması verilen , teminat süresi genellikle 12 aydır.</p>
                        <p>Teminat verilen bedel Makine-Cihazların yeni alım bedelidir.</p>

                        <h4>Elektronik Cihaz Sigortası</h4>
                        <p>Elektronik cihaz sigortası, elektronik cihazların çalışırken veya aynı iş yerinde temizleme, yer değiştirme esnasında ani ve beklenmedik her türlü sebepten dolayı oluşabilecek hasarların giderilmesi için gereken tamirat ve ikame masraflarını karşılar.</p>
                        <p>Elektronik cihaz sigortası teminat altına alınan riskleri ek prim alarak genişletmek mümkündür. Bu anlamda fiziki infilak temel ve kaideler, fazla mesai ve seri vasıtalar, grev, lokavt, kargaşalık, halk hareketleri, terör gibi teminatları poliçeye eklemek mümkündür.</p>
                        <p>Bu sigorta ile verilen teminat genellikle 12 ayla sınırlıdır.</p>
                        <p>Sigorta bedeli, elektronik cihazların yeni alım bedelidir.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection