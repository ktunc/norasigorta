@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Zorunlu Trafik Sigortası Nedir?</h4>
                        <p>Zorunlu Trafik Sigortası, 2918 sayılı Karayolları Trafik Kanununda düzenlenen ve trafiğe çıkan her aracın yaptırmak zorunda olduğu bir sigorta çeşididir. Her yıl yenilenmek zorundadır. Zorunlu trafik sigortası, her araç sahibi tarafından yaptırılması zorunlu olan ve hasar anında karşı tarafa verilebilecek bedeni ve maddi zararları güvence altına alan bir sigortadır. Zorunlu trafik sigortasını yaptırmayan araçlar trafiğe çıkamazlar. Bu sigorta çeşidi Türkiye Cumhuriyeti sınırları içerisinde geçerlidir. Zorunlu trafik sigortası yapıldığı aracın zararını karşılamaz, sadece zarar verdiği diğer aracın zararını karşılar.</p>
                        <p>Zorunlu trafik sigortası olmayan ya da süresi sona ermiş araçlar görüldükleri yerde trafiğe çıkması engellenecek şekilde işlem yapılır (bağlanır). Her araç sahibi zorunlu trafik sigortasını yaptırmak ve sigorta süresi sona ermeden yeniletmekten sorumludur. Zorunlu olan bu sigortayı yaptırmayanların araçları trafik şubelerinin otoparklarına alınır. Yani bir ceza ödeyerek yola devam etmek mümkün değildir. Araç anında trafikten men edilir. Bunun sonrasında aracı geri almak pek kolay değildir. Aracı almak için trafik sigortasının mutlaka yapılması gerekir. Aracın bulunduğu otoparka gidilerek görevli memurlara durum haber verilir. Belgenin tebliğ edilmesi şarttır. Bundan sonra da araç sigortasız olarak trafiğe çıktığı için para cezası ödenir. Aracın otoparkta kaldığı her gün için de otoparka ücret ödenmesi gerekmektedir.</p>
                        <p>Zorunlu trafik sigortasındaki limitler trafik kazasında oluşacak zararın türüne göre değişir. Herhangi bir can kaybı olması durumunda ölenin yakınlarına verilmesi gereken miktar, yaralanma olması durumunda hastane masrafları için ödenecek miktardan farklıdır. Aynı şekilde sadece maddi hasar olması durumunda da ödenecek rakam farklılık gösterir. Böyle durumlarda ödenecek miktar her sene değişmekte ve böylece her sene ödenecek primlerde de değişiklik olmaktadır.</p>

                        <h4>Trafik Sigortası Teminatları Nelerdir?</h4>
                        <p>Zorunlu trafik sigortası teminatları ve limitleri Hazine Müsteşarlığı tarafından belirlenen, bütün sigorta şirketleri için aynı olan teminatlardır. Ancak fiyat ve ödeme koşulları şirketlere göre farklılık göstermektedir.</p>
                        <p>Zorunlu trafik sigortası kapsamında, sigorta teminatları ve limitleri, araçların model yılı ve ruhsat sahibinin mesleği gibi değişkenler nedeniyle bazı farklılıklar oluşur.</p>

                        <h4>Trafik sigortası kapsamı altında verilen teminatlar şu şekildedir:</h4>
                        <ul>
                            <li>Kişi başı vefat ve tedavi teminatı; Hasarın yaşanması halinde her kişi için verilecek olan vefat ve tedavi masrafları,</li>
                            <li>Kaza başına vefat ve tedavi teminatı; Bir hasar durumunda bir kazadaki her bir kişinin vefat ve tedavi masraflarını karşılar,</li>
                            <li>Araç ve kaza başına maddi hasar teminatı; Bir hasar durumunda karşı tarafa verilecek maddi zararları karşılar,</li>
                        </ul>
                        <p>Trafik sigortası, zorunlu olmasının yanı sıra, karşı tarafa verilen hasarları karşılaması bakımından araç sahiplerini maddi olarak koruyan bir sigortadır.Gerek kaza olması ihtimali, gerekse herhangi bir polis kontrolünde ilk dikkat edilecek unsurlardan biri olan trafik sigortası ihmal edilmemelidir.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection