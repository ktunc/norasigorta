@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Kasko Nedir?</h4>
                        <p>Kasko mal sigortalarının türlerinden biridir. Herhangi bir motorlu kara taşıtının sigortalının iradesi dışında hasara uğraması, yanması, çalınması v.b. durumlarda, sigortalıya tazminat ödenmesini sağlamak için yapılır.</p>
                        <p>Kasko karşı araçtaki hasarı değil, sigortalıya ait araçtaki hasarı teminat altına alır. Araç kasko sigortasının amacı, sigorta ettirene ait motorlu aracın uğrayacağı zararları tazmin etmektir.</p>

                        <h4>Kasko Sigortası Kapsamı</h4>
                        <p>Kasko isteğe bağlı bir sigorta türüdür. Aracın yanması, çalınması gibi standart teminatlar dışında sigortalının talebi doğrultusunda ek teminatlar ile sigorta kapsamı genişletilebilir. Poliçede aksi belirtilmediği sürece kasko sadece Türkiye sınırları içinde geçerlidir.</p>

                        <h4>Kasko Sigortasında Başlangıç ve Bildirim Süreleri</h4>
                        <p>Kasko sigortası ödemenin yapıldığı tarihte başlar. Eğer poliçe tutarı taksitle ödenecekse peşinatın ödendiği gün sigorta başlar. Kasko sigortasının başlangıcı ve bitişi taraflar arasında aksi kararlaştırılmadıkça, Türkiye sınırları içinde, Türkiye saati ile 12.00’de başlar ve bitim günü aynı saatte sona erer. Kasko sigortasında, sigorta ettiren araca zarar verici olayın yaşandığı günden itibaren 5 iş günü içinde rizikonun gerçekleştiğini sigortacıya bildirmekle yükümlüdür.</p>
                        <p>Rizikonun gerçekleşmesi halinde, sigortacı hasar ile ilgili belgelerin kendisine verilmesinden itibaren en geç 15 gün içerisinde hasar ve tazminat miktarını sigortalıya bildirmek zorundadır.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection