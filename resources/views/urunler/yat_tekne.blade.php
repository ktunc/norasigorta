@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Tekne/Yat Sigortaları</h4>
                        <p>Denizlerde güvenli bir yolculuk için Tekne/Yat Sigortaları yaptırın, yatınız ve içindeki ekipmanları olası risklere karşı güvence altına alalım...</p>

                        <h4>Gezinti Teknesi Sigortaları</h4>
                        <p>Yat Sigortası ile yatın gövdesi, makineleri, adını taşıyan servis botları, yat ile birlikte alınıp satılması mutad olan donanım ve ekipmanı Institute Yacht Clauses 1.11.82 Cl.328 kapsamında tek bir poliçe ile güvence altına alınmaktadır.</p>
                        <p>Sigorta güvencesi, yatın poliçe içinde belirtilmiş olan coğrafi alan içerisinde bulunması kaydıyla, denizde, yatma mahallinde, çekek yerinde, çekeğe alınırken veya çekekten indirilirken kesintisiz olarak devam eder.</p>

                        <h4>Başlıca aşağıdaki risklere karşı sigortalıdır:</h4>
                        <ul>
                            <li>Tüm deniz kaza ve tehlikeleri (çarpma, çatma, karaya oturma, alabora olma, fırtına)</li>
                            <li>Yangın, yıldırım, infilak</li>
                            <li>Deprem, volkanik patlama</li>
                            <li>Korsanlık</li>
                            <li>Hava taşıtları ile temas</li>
                            <li>Yatın veya servis botlarının çalınması</li>
                            <li>Makine veya teçhizatın zor kullanılarak çalınması</li>
                            <li>Yatın sebep olduğu bir kaza neticesinde 3. kişilerin can ve mal kaybı (tekne sigorta bedeliyle sınırlı olmak üzere)</li>
                            <li>Enkaz kaldırma masrafları (Tekne sigorta bedeliyle sınırlı olmak üzere)</li>
                            <li>Kötü niyetli hareketler (kaptan ve gemi adamlarının barataryası dahil)</li>
                        </ul>
                        <p>Savaş, grev, kargaşalık, halk hareketleri teminatları ek prim karşılığında ilave edilebilir.</p>

                        <h4>3. şahıslara karşı sorumluluklar da yat sigortası ile teminat altına alınır:</h4>
                        <ul>
                            <li>Yatın çarpıştığı tekne veya üzerindeki mala gelebilecek kayıp ve hasarlar,</li>
                            <li>Yatın liman, dok, iskele, rıhtım, fener, telefon, telgraf kabloları gibi diğer sabit ve hareketli cisimlere vereceği zararlar,</li>
                            <li>Yatta veya başka bir teknede meydana gelebilecek can kaybı, sakatlanmalar, can kurtarma masrafları,</li>
                            <li>Enkaz kaldırma masrafları</li>
                        </ul>
                        <p>geniş teminatlı yat poliçesi tarafından karşılanmaktadır.</p>

                        <h4>Teminat kapsamı dışında kalan riskler:</h4>
                        <ul>
                            <li>Yatın adını ve işaretini taşımayan servis botları,</li>
                            <li>Zati eşyalar, yakıt, kumanya, balıkçılık takımları, demirleme donanımları,</li>
                            <li>Dıştan takma motorların düşmesi,</li>
                            <li>Dizayndaki veya konstrüksiyondaki hataların giderilmesi veya iyileştirilmesi veya dizaynının veya konstrüksiyonunun değiştirilmesi için yapılan harcamalar veya uğranılan kayıplar,</li>
                            <li>Nükleer rizikolar,</li>
                            <li>Savaş ve benzeri rizikolar, grev-lokavt, halk hareketleri, terör (aksine sözleşme ve ilave prim ile temin edilebilirler.)</li>
                            <li>Yatın mutad olmadıkça veya yardıma muhtaç durumda bulunmadıkça yedekte çekilmesi veya başka bir tekneyi yedeğine alması veya kurtarma ve yardımda bulunmasını doğuran bir sözleşme yapılması halleri Sigortacının onayı alınmadıkça yatın deniz evi olarak kullanılması veya esaslı bir tamirat veya tadilata tabi tutulması,</li>
                            <li>Ayrıca temin edilmedikçe, yatın su kaynağı ve benzeri sporların yapılmasında kullanılması nedeniyle doğabilecek sorumluluklar</li>
                        </ul>
                        <p>sigorta kapsamına girmez.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection