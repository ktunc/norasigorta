@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Hayat Sigortası teminatlarım neler?</h4>
                        <p>Hayat sigortaları, öncelikle ana teminat olan vefat teminatını içermektedir. Vefat teminatı; sigortalının başına istenmeyen bir olay gelmesi sonucunda, vefat etmesi durumunda devreye girmektedir. Hayat sigortaları, sigortalının önceden belirtmiş olduğu lehtar ya da kanuni varislerine sigorta poliçesinde belirtilen teminatları ödeyerek mali güçlüklerin yaşanmasını engellemektedir.</p>

                        <h4>Neden Hayat Sigortası yaptırmalıyım?</h4>
                        <ul>
                            <li>Herhangi bir sakatlık durumunda hayatınızı aynı şekilde devam ettirmenizi sağlar.</li>
                            <li>Vefat durumunda sevdiklerinize bırakabileceğiniz parasal bir varlık niteliği taşır. Eğitimini devam ettirmekte olan çocuklarınız varsa eğitimlerinin yarım kalmamasını sağlar.</li>
                            <li>Poliçe kapsamına giren bir kaza sonucunda ortaya çıkan yüksek tutarlı tedavi giderlerinizin ödenmesini düşük primlerle sağlar.</li>
                            <li>Beklenmedik bir anda tehlikeli bir hastalığa yakalanılması durumunda kişilere finansal destek sağlar.</li>
                            <li>Ödenen primler yürürlükteki vergi kanunlarına göre belirli miktara kadar gelir vergisi matrahından düşülebilir.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection