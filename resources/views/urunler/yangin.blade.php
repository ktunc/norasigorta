@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Yangın Sigortası Nedir?</h4>
                        <p>Bu sigorta ile, işyeri veya konut binanızı, muhteviyatınızı (demirbaş, makine-tesisat, emtea vb.) seçtiğiniz teminatlar çerçevesinde sigorta güvencesi altına alabilirsiniz. Ek teminatlar seçerek poliçe kapsamınızı genişletebilir, ihtiyaçlarınıza en uygun alternatifi yaratabilirsiniz.</p>

                        <h4>Yangın Sigortası Teminatları</h4>
                        <p>Yangın Sigortasının güvence altına aldığı riskler</p>

                        <h4>Yangın</h4>
                        <p>Herhangi bir nedenle meydana gelen yangın sonucu ya da söndürme çalışmaları sırasında oluşabilecek maddi hasarlar. Hatta bu yangın komşunuzda başlasa bile, sizin evinizde oluşacak hasarlar.</p>

                        <h4>Yıldırım</h4>
                        <p>Yıldırım düşmesi sonucu yangın çıksın veya çıkmasın meydana gelen zararların teminat kapsamına alınmasıdır.</p>

                        <h4>İnfilak</h4>
                        <p>Havagazı veya doğal gaz tesisatının bütan, oksijen gibi her türlü gaz tüplerinin patlamasıyla ısı tesisatında meydana gelebilecek infilak sonucu oluşabilecek zararlar bu teminat kapsamına dahildir.</p>

                        <h4>Dahili Su Basması</h4>
                        <p>Bina dahilindeki su depo ve sarnıçlarının, su borularının kalorifer kazanı radyatör ve borularının su tesisatının patlaması, taşması, sızması, tıkanması, kırılması ve donması ile doğrudan doğruya meydana gelen zararlar, yağmur sularının, kar veya buzların erimesi neticesi meydana gelen suların çatı ve saçaktan sızması, su oluklarının tıkanması veya taşması sonucu binaya giren suların meydana getirdiği zararlar, don neticesi tesisatta ve buna bağlı cihazlarda meydana gelen zararlar ile tamir maksadıyla yapılan duvarın açılması ve kapanması masraflarının temin edilmesidir.</p>

                        <h4>Fırtına</h4>
                        <p>Yağmur, kar, dolu ile beraber olsun veya olmasın fırtına sonucu veya esnasında rüzgarın sürüklediği veya attığı şeylerin çarpması sonucu sigortalı şeylerde doğrudan meydana gelecek zararların temin edilmesidir.</p>


                        <h4>Yer Kayması</h4>
                        <p>Sigortalı binanın inşa edilmiş olduğu arsada veya civarında vukuu bulan yer kayması veya toprak çökmesi sonucu sigortalı şeylerde doğrudan meydana gelecek zararlar ile sel veya su baskını nedeniyle meydana gelen yer kaymasıdır.</p>

                        <h4>Duman</h4>
                        <p>Bir boru veya menfezle, bacaya bağlanmış ısıtma ve pişirme cihazlarının münhasıran ani, olağan dışı veya kusurlu şekilde işlemesi yüzünden çıkan dumanın, sigorta konusu kıymetlere doğrudan sebebiyet vereceği ziya ve hasarların teminat altına alınmasıdır.</p>

                        <h4>Kara Taşıtları Çarpması</h4>
                        <p>Motorlu ve motorsuz kara taşıt araçlarının sigortalı şeylere çarpması sonucu doğrudan meydana gelecek zararların teminata ilave edilmesidir.</p>

                        <h4>Hava Taşıtları Çarpması</h4>
                        <p>Uçakların ve diğer hava taşıtlarının çarpma, düşme ve bunlardan bir parçanın veya cismin düşmesi sonucu oluşabilecek ziya ve hasarların teminat kapsamına alınmasıdır.</p>

                        <h4>Deniz Araçları Çarpması</h4>
                        <p>Deniz Araçlarının Çarpması sonucunda sigortalı kıymetlerde meydana gelen zararların teminat altına alınmasıdır.</p>

                        <h4>Hırsızlık</h4>
                        <p>Riziko mahallinde bulunabilecek eşyaların kırmak, zorlamak, anahtar uydurmak veya şiddet kullanmak yolu ile çalınması ve hırsızlık fiilinin gerçekleşmesi sonucunda işyerinde meydana gelebilecek tahribat bu teminat ile dahil edilir.</p>

                        <h4>Kar Ağırlığı</h4>
                        Yoğun kar yağışından sonra çatı üzerinde biriken karın ağırlığının nedeniyle sigorta konusu bina ve içindeki şeylerde doğrudan meydana gelecek zararlardır.

                        <h4>Grev, Lokavt, Kargaşalık, Halk Hareketleri ve Kötü Niyetli Hareketler, Terör</h4>

                        <p>Yangına sebebiyet vermiş olsun, olmasın grev, lokavt, iş anlaşmazlığı, kargaşalık veya halk hareketleri sırasında meydana gelen olaylar ve bu olayları önlemek ve etkilerini azaltmak üzere yetkili organlar tarafından yapılan müdahaleler sonucu sigortalı şeylerde doğrudan meydana gelen bütün zararlar teminata ilave edilmiştir.</p>
                        <p>Kötü Niyetli Hareketler, Grev,Lokavt, Kargaşalık, Halk Hareketleri klozunda belirtilen olaylarla ilgili olmaksızın, sigortalı ve sigortalının usul ve füruu dışındaki herhangi bir kimsenin, kötü niyetli hareketiyle bu olayları önlemek ve etkilerini azaltmak üzere yetkili organlar tarafından yapılan müdahaleler sonucu sigortalı şeylerde doğrudan meydana gelen yangın ve infilak sonucu hariç, bütün zararlar teminata ilave edilmiştir.</p>
                        <p>Terör, Terörist eylemler ve bu eylemlerden doğan sabotaj ile bunların gerektirdiği askeri ve inzibati tedbirlerin sebep olduğu yangın ve infilak sonucu meydana gelen dahil bütün zararlar, teminata ilave edilmiştir.</p>

                        <h4>Enkaz Kaldırma Masrafları</h4>
                        <p>Yangından sonra yıkıntıların kaldırılması ve taşınmasıyla ilgili masraflar teminat dahilindedir. Mühendislik Sigortalarında ise inşaat veya montajda oluşabilecek herhangi bir hasarın sonucundaki enkaz kaldırma masraflarının teminat altına alınmasıdır.</p>

                        <h4>Deprem ve Yanardağ Püskürmesi</h4>
                        <p>Deprem yer altındaki boşlukların çökmesi, volkanik indifa ve özellikle toprağın içersinde ani enerji boşalması nedenleri ile yer kabuğunun tabi olarak şekil değişikliğine uğramasıdır. Deprem ve Yanardağ püskürmesinin doğrudan veya dolaylı neden olacağı yangın ve infilak sonucu meydana gelen dahil bütün zararların temin edilmesidir.</p>

                        <h4>Sel ve Su Baskını</h4>
                        <p>Sigorta konusu, sigorta civarındaki nehir, ırmak, çay, dere ve kanalların taşması, denizlerin gel-git olayları dışında kabarması, olağanüstü yağışlar sebebiyle husule gelen sel ve seylap neticesi tesisi hariçten istila eden suların doğrudan doğruya vereceği zararların teminat kapsamı içine alınmasıdır.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection