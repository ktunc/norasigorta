@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Tıbbi Kötü Uygulamaya Yönelik Zorunlu Mali Sorumluluk Sigortası</h4>
                        <p>T.C. sınırları içinde sağlık hizmeti veren yerlerde fiili olarak çalışan hekimlerin ve diğer sağlık personelinin yapmış olduğu faaliyetleri poliçede belirtilen uzmanlık sıfatı altında, sigorta süresi içinde, meslekleri ile ilgili gerçekleştirecekleri faaliyetlerini yerine getirirken üçüncü şahıslara verebilecekleri her türlü bedeni zararı karşılayan ve hekim içinde kaza sigortası niteliği taşıyan sigorta türüdür.</p>

                        <h4>İşveren Mali Sorumluluk Sigortası</h4>
                        <p>İş yerinde görevini yaparken iş kazası geçiren çalışanların yaralanma ve ölmesi halinde kanuni varislerinin talep edeceği tazminatlara ve kazadan dolayı tazminat ödemek zorunda olan SSK’nın ödediği tazminatı işverenden talep etmesi durumuna karşı işvereni de güvence altına alır.</p>

                        <h4>Mesleki Sorumluluk Sigortası</h4>
                        <p>Sigortalının mesleğinden doğan hata ve kusurlar sonucunda meydana gelebilecek zarardan doğan tazminat taleplerini teminat altına alır. Bu sigortada, mimar ve mühendisler, avukatlar, serbest muhasebeciler, mali müşavirler ve patent-marka vekillerine teminat verilir.</p>

                        <h4>Özel Güvenlik Zorunlu Mali Sorumluluk Sigortası</h4>
                        <p>Özel güvenlik görevlilerinin hizmetlerine dair mevzuat çerçevesinde görevlerini yerine getirirken üçüncü şahıslara verebilecekleri her türlü zarar ve ziyanları teminat altına alır. Güvenlik görevlilerinin bulundukları adreste, faaliyet gösterdikleri şirket ve kaç kişi olarak hizmet verdikleri bilgisine göre yasal olarak teminat limitleri değişiklik gösterir.</p>

                        <h4>Yehlikeli Maddeler Zorunlu Sorumluluk Sigortası</h4>
                        <p>Her türlü yanıcı, patlayıcı ve yakıcı maddeleri üreten, kullanan, depolayan, nakleden ve satanların faaliyetler sırasında bu maddelerin doğrudan veya dolaylı olarak sebep olduğu olaylar neticesinde üçüncü şahıslara verilecek bedeni ve maddi zararlara karşı sorumlulukları teminat altına alınır.</p>

                        <h4>Tüpgaz Zorunlu Sorumluluk Sigortası</h4>
                        <p>LPG tüpleyen firmaların doldurdukları veya doldurttukları yetkili bayileri vasıtasıyla ya da doğrudan tüketiciye intikal ettirdikleri tüplerin kullanılmak üzere bulundurdukları yerlerde patlaması, gaz kaçırması ve yangın çıkarması sonucu verecekleri her türlü bedeni ve maddi zararları teminat altına alır.</p>

                        <h4>Üçüncü Şahıslara Karşı Mali Sorumluluk Sigortası</h4>
                        <p>Sigortalının gerek iş yerine gerekse konutuna gelen üçüncü kişilere her hangi bir sebeple vereceği bedeni veya maddi zararları teminat altına alır.</p>

                        <h4>Ürün Mali Sorumluluk Sigortası</h4>
                        <p>Sigortalı tarafından satılan tedarik edilen tesis ve monte edilen veya işlenen her hangi bir ticari mal veya üründen kaynaklanan kusur neticesinde üçüncü şahısların uğrayacağı bedeni ve maddi hasarları teminat altına alır.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection