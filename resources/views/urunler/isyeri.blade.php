@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>İş Yeri Sigortaları</h4>
                        <p>İşyeri sigorta ile, işyeri binanızı, muhteviyatınızı (Demirbaş, makine-tesisat, emtea vb.) ve camlarınızı, paket olarak bir araya getirilmiş otomatik teminatlar çerçevesinde sigorta güvencesinde. Ayrıca poliçe dahilinde çalışanlarınız için Ferdi Kaza Teminatı da sağlayabilirsiniz.</p>
                        <p><b>Hırsızlık:</b> Hırsızlık sonucu çalınan emtea ve sabit kıymetler, hırsızın işyerine girerken verebileceği zararlar.</p>
                        <p><b>Grev, Lokavt, Terör:</b> Grev, lokavt, halk hareketleri, terörist eylemler ve kötü niyetli hareketler sonucunda ve bu hareketleri önlemek amacıyla yetkililerce yapılan müdahaleler nedeniyle işyerinde meydana gelebilecek hasarlar.</p>
                        <p><b>Yer Kayması:</b> Yer kayması ve toprak çökmesi nedeniyle işyerinde meydana gelebilecek her türlü hasar.</p>
                        <p><b>Deprem:</b> Deprem nedeniyle işyerinde meydana gelebilecek her türlü hasar.</p>
                        <p><b>Yıldırım:</b> Yangın çıksın ya da çıkmasın, yıldırım düşmesi sonucunda oluşabilecek hasarlar.</p>
                        <p><b>Yangın:</b> İşyerinde ya da yakınında çıkabilecek bir yangının yayılması sonucunda alevlerin ve dumanın binaya ya da eşyalara verebileceği zararlar.</p>
                        <p><b>İnfilak:</b> Doğalgaz, tüpgaz, havagazı ve kalorifer kazanı tesisatının infilakı sonucu oluşabilecek hasarlar.</p>
                        <p><b>Fırtına:</b> Fırtına ya da fırtına sırasında rüzgarın sürüklediği veya attığı cisimlerin çarpması sonucu oluşabilecek hasarlar.</p>
                        <p><b>Dahili Su ve Sel Baskını:</b> Su tesisatının patlaması, sular kesildiğinde sigortalının ya da komşularının musluklarını açık bırakması sonucu doğabilecek hasarlar. Sel ya da su baskınında işyerinin uğrayabileceği zararlar.</p>
                        <p><b>Kar Ağırlığı:</b> Çatıda biriken kar ağırlığı nedeniyle oluşabilecek zararlar.</p>
                        <p><b>Cam Kırılması:</b> İşyerinde bulunan kapı ve pencere camlarının kırılması sonucu meydana gelebilecek hasarlar.</p>
                        <p><b>Enkaz Kaldırma:</b> Teminatlardan doğabilecek hasarların limitler içinde oluşturduğu enkazın kaldırma masrafları.</p>
                        <p><b>Araç Çarpması:</b> Her türlü kara, hava ve deniz aracının işyerine çarpması halinde meydana gelebilecek hasarlar. Duman: Isıtma ve pişirme cihazlarının arızasından kaynaklanan dumanın, doğrudan verebileceği zararlar.</p>
                        <p><b>Komşulara Karşı Sorumluluk:</b> Yangın, dahili su hasarları nedeniyle komşulara karşı sorumluluk.</p>
                        <p><b>Üçüncü Şahıslara Karşı Sorumluluk:</b> Sigortalının, işyerinde 3. Şahıslara verebileceği maddi ve bedeni zararlardan doğan hukuki sorumlulukları.</p>
                        <p><b>Mal Sahibine Karşı Sorumluluk:</b> Bina sigortalıya ait değilse, o işyerinde kiracı olarak bulunuluyorsa, yangın, dahili su, ve duman hasarlarının gerçekleşmesi sonucunda mal sahibine karşı sorumluluk.</p>
                        <p><b>Kira Kaybı:</b> Yangın hasarı nedeniyle, işyerinin kullanılamayacak duruma gelmesi halinde, sigortalı kiracı ise ve kira bedelini peşin ödemiş ise, işlememiş sürenin kira karşılığı; sigortalı mal sahibi ise, işyerinin boş kalması sonucunda alınamayan kira gelirinden doğabilecek kayıplar.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection