@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Konut Sigortaları</h4>
                        <p>Konut sigortası evinizi ve ailenizi koruma altına alan en önemli sigortadır. Konut sigortaları evinizi yıkılmalardan, hırsızlıktan, terörden ve kazalardan korumayı amaçlayan bir sigorta çeşididir. Yeni gelen günün ne getireceği bilinmeyeceğinden konut sigortası yaşanabilecek risklere karşı size teminat altına alır. Evinizin bir teminat altında olduğunu bilmek evden uzaklaştığınız her an size güvende hissettir. Aylık taksitler ile evinizi sigorta altına alabilirsiniz. Bu sigortalar yıllık olarak yapılır. Her sene yenilenebilir. Uzun süre aynı sigorta şirketinden konut sigortası yapıyorsanız ödemelerde indirimler de söz konusu olacaktır.</p>
                        <p>Evinizde yukarıda söz edilen herhangi bir hasarın gerçekleşmesi durumunda çoğunlukla eşyalar da zarar görecektir. Bu yüzden sigorta yaptırılırken konutların içindeki eşyalarla beraber sigorta kapsamına alınması gereklidir. Az bir maliyete daha katlanılarak eşyalar da güvence altına alınabilir.</p>
                        <p>Konut sigortası yaptırmak için illaki ev sahibi olmanız gerekmez, kiracı olunması durumunda da sadece eşya teminatı alınarak eşyalar güvence altına alınabilir.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection