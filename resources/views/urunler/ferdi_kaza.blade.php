@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Ferdi Kaza Sigortası Nedir?</h4>
                        <p>Herkes gibi, hayatımızı güvenli bir şekilde yaşamak isteriz. Fakat maalesef, hiç olmadık zamanlarda başımıza gelmesini istemediğimiz durumlarla da karşılaşabiliyoruz. Sigorta poliçesi süresince gerçekleşen herhangi bir kaza sonucu sizi koruyan bir güvencedir. Kaza sonucu oluşabilecek tedavi masraflarını karşılamanın yanında, kaza sonucu eğer sigortalı sakat kalırsa kendisine; vefat ederse lehtarına güvence sağlar.</p>

                        <h4>Ferdi Kaza Sigortası’nın teminatları nelerdir?</h4>
                        <ul>
                            <li>Beklenmedik ve ani kazalarda ölüm.</li>
                            <li>Sürekli sakatlık.</li>
                            <li>Tedavi masrafları.</li>
                        </ul>

                        <h4>Ferdi Kaza Sigortasından kimler yaralanabilir?</h4>
                        <p>Ferdi Kaza Sigortası’ndan 18-65 yaş arası herkesin yararlanması mümkündür.</p>

                        <h4>Süresi ne kadardır?</h4>
                        <p>Ferdi kazasının sigortasının süresi 1 yıldır.</p>
                        <p>Ferdi kaza sigortası tek başına yaptırılabileceği gibi, pek çok paket sigorta ürünü içerisinde belli limitler dahilinde de sunulur. Örneğin, kasko ve konut sigortalarında ferdi kaza teminatı ek teminat olarak yer alır. Bir kaza durumunda mevcut kasko ve konut poliçenizin özel şartlarını da kontrol edin. Bu poliçelerinizdeki ferdi kaza sigortası teminatıyla kendinize güvence sağlayabilirsiniz.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection