@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Seyahat Sigortası Nedir?</h4>
                        <p>Seyahat sigortası hem yurt içi, hem yurt dışı seyahatlerde karşılaşabilinecek hastalık, kaza, valiz kaybı gibi riskleri güvence altına alan bir sigorta çeşitidir. Yurtdışı seyahat sigortası özelikle Avrupa ülkeleri vize işlemlerinin bir önkoşulu olması nedeniyle (Schengen vizesi) “vize sigortası” olarak da bilinir.</p>
                        <p>Seyahat sigortası satın alırken, poliçenizin başlangıç ve bitiş tarihlerine, seyahat edecek kişi sayısına, ziyaret edeceğiniz ülkenin varsa vize şartlarına uygunluğuna, dikkat etmeniz çok önemlidir.</p>
                        <p>Seyahat sigortası farklı gün ve sürelerde olabilir. En az bir haftalık, en çok 1 yıllıktır. Ücretlendirme sistemi de bu doğrultuda değişiklik gösterir.</p>

                        <h4>Seyahat Sigortası Teminatları Nelerdir?</h4>
                        <p>Standart bir seyahat sigortası <b>30.000</b> Euro sigorta teminatı kapsamına sahiptir. Seyahatiniz sırasında oluşabilecek hasarlarda, tarafınıza ödenecek miktar ve alacağınız hizmetler, belirleyeceğiniz teminatlara göre değişkenlik göstermektedir. Sigorta teminatı da sigorta şirketlerine ve ürün özelliklerine göre farklılık gösterir. Bu nedenle doğru sigorta teminatına sahip bir poliçe seyahatiniz esnasında rahat etmeniz açısından gerekli ve önemlidir.</p>

                        <p>Yurtdışı seyahat sigortası, yurt dışına çıkış yapıp vize aldığınız ülkenin gümrüğünden geçmenizle başlayan, sonra Türkiye’ye giriş yapmanızla sona eren süreler arasında geçerlidir.</p>
                        <p><b>Seyahat sigortası teminatları temel olarak şunlardır:</b></p>
                        <ol>
                            <li>Kaza sonucu vefat teminatı,</li>
                            <li>Kaza sonucu sürekli sakatlık teminatı,</li>
                            <li>Yurtdışındaki seyahati süresince hastalık veya yaralanma durumunda tıbbi tedavi giderleri,</li>
                            <li>Seyahat sırasında hastalanma veya yaralanma halinde uygun bir tıbbi merkeze nakil giderleri,</li>
                            <li>Acil ilaç getirilmesi veya gönderilmesi giderleri,</li>
                            <li>Yurtdışı seyahati sırasında hastalık veya yaralanma nedeniyle konaklama süresinin uzatılmasına bağlı giderleri,</li>
                            <li>Konaklama süresinin uzadığı durumlarda aile üyelerinden birinin hastanenin bulunduğu yere seyahat giderleri,</li>
                            <li>Seyahat halindeyken sigortalının evinde meydana gelebilecek bir hasar sebebiyle eve geri dönüş masrafları,</li>
                            <li>Seyahat süresince yakın bir aile üyesinin vefatı nedeniyle acil geri dönüş masrafları,</li>
                            <li>Yolculuk süresinde vefat halinde cenazenin yurda nakli ve eşlik edenlerin geri dönüş organizasyonu ve giderleri,</li>
                            <li>Teminat konusu tüm haller ile ilgili acil mesajların gönderilmesi giderleri,</li>
                            <li>Seyahat esnasında kayıp bagajın bulunması halinde sigortalının evine veya seyahat için tasarlanan varış yerine gönderilmesi giderleri,</li>
                            <li>Tarifeli uçuşlarda gecikmeli bagajın tazmini</li>
                        </ol>

                        <h4>Seyahat Sigortası Yaptırırken Dikkat Edilmesi Gerekenler Neler?</h4>
                        <p>Yurt dışına çıkarken pek çok ülke için zorunlu olarak yaptırdığınız seyahat sigortası hakkında yeterince bilgi sahibi misiniz? İşte seyahat sigortası yaptırırken dikkat etmeniz gereken detaylar:</p>
                        <p>Seyahat sigortası genel şartları devlet tarafından belirlenir. Seyahat sigortalarının şartları ve tipleri bu yüzden aynıdır.</p>
                        <p>Seyahat sigortasında teminatlar için iki seçenek bulunur. İlk seçenekte, toplamda 100 gün teminat alırsınız, ancak bu teminat seyahat başına 60 gün ile sınırlıdır. İkinci seçenekte ise toplam süre limiti yoktur, ancak seyahat başına 90 gün limit bulunur. Bu sürelerin sonunda seyahat sigortanızın geçerliliğinin devam etmesi için yurda giriş yapmanız gerekir.</p>
                        <p>Elle yazılan poliçeler bazı konsolosluklarda kabul edilmemektedir.</p>
                        <p>Yurtdışında, seyahat sigortası kapsamında sağlık hizmeti almanız gerektiğinde durumu 24 saat içerisinde sigorta şirketinize bildirmeniz gerekir. Aksi takdirde ödemeniz reddedilebilir.</p>
                        <p>Yurtdışında da geçerli olan bir özel sağlık sigortası poliçesi sahibiyseniz, poliçenizi İngilizce’ye çevirttikten sonra onaylatmayı deneyebilirsiniz.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection