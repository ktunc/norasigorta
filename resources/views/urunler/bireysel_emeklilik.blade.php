@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Bireysel Emeklilik Sistemi Nedir?</h4>
                        <p>Bireysel Emeklilik Sistemi (BES), bireylere emeklilik döneminde ek bir gelir sağlamayı amaçlayan özel bir tasarruf ve yatırım sistemidir. Mevcut kamu sosyal güvenlik sisteminin bir alternatifi değil, tamamlayıcısıdır. Sisteme fiil ehliyetine sahip 18 yaş üzerindeki herkes katılabilir, herhangi bir sosyal güvenlik kurumuna bağlı olma ya da çalışma şartı aranmaz. Gönüllü katılıma dayalı bir sistemdir. Sistemin amacı, kişileri çalışma yaşamları boyunca emeklilik dönemlerindeki nakit ihtiyaçlarını karşılamaya yönelik tasarrufa özendirmek, uzun vadeli birikim yapmasını teşvik etmek, bu sayede aktif çalışma süresi boyunca sahip oldukları gelir seviyesini emeklilik döneminde de sürdürebilmesini sağlamaktır.</p>

                        <h4>Bireysel Emeklilik Planı İçin Ne Kadar Katkı Payı Ödenmelidir?</h4>
                        <p>Mevcut gelir durumunuzu ve emeklilik dönemine ait beklentilerinizi de göz önünde bulundurarak yatırmak istediğiniz katkı payı tutarınızı seçeceğiniz planda tanımlı olan plan katkı payından az olmamak üzere belirleyebilirsiniz. Belirlediğiniz katkı payı tutarını aylık, 3 aylık, 6 aylık ve yıllık olarak yatırabilirsiniz. Emeklilik döneminizde daha yüksek birikime sahip olmak için bugünkü gelirinizin %5 - %15’i oranında katkı payı belirleyebilir, katkı paylarınızı düzenli ödeyerek, ara dönemlerde ek ödemeler yaparak, katkı payı tutarınızı artırarak ve fon getirilerinizi takip ederek elde edeceğiniz birikimi yüksek seviyelerde tutabilirsiniz.</p>

                        <h4>Bireysel Emeklilik Sisteminde Birikimler Nasıl Değerlenir?</h4>
                        <p>Birikimleriniz emeklilik yatırım fonlarında değerlendirilir. Emeklilik şirketleri tarafından kurulan bu fonlar uzman portföy yönetim şirketleri tarafından yönetilir. Böylece yapacağınız düzenli tasarruflarınız için diğer yatırım fonlarında olmayan vergi avantajından ve fon yönetim şirketlerinin kurumsal hizmetlerinden yararlanırsınız.</p>

                        <h4>Bireysel Emeklilik Sisteminin En Önemli Avantajı Nedir?</h4>
                        <p>Bireysel Emeklilik Sistemi, başka hiçbir yatırım aracında bulunmayan devlet katkısı avantajına sahiptir. Sisteme yapacağınız ödemeler üzerinden %25 oranında devlet katkısı* alırsınız. Yani her 100 TL'lik ödemeniz için 25 TL devlet katkısı kazanırsınız.</p>

                        <h4>Diğer Avantajları Nelerdir?</h4>
                        <ul>
                            <li>Her bütçeye uygun farklı planlardan dilediğinizi seçebilirsiniz.</li>
                            <li>Ödeme periyodunu (1 aylık, 3 aylık, 6 aylık, yıllık) kendiniz belirleyebilirsiniz.</li>
                            <li>Ödemelerinizi Garanti Bankası kredi kartlarıyla yapmanız durumunda özel avantajlardan faydalanabilirsiniz.</li>
                            <li>Dilediğiniz zaman ödemelere ara verebilir, istediğinizde tekrar kaldığınız yerden devam edebilirsiniz.</li>
                            <li>Geleceğiniz için kesenize ve gönlünüze göre bir emeklilik planlamış olursunuz.</li>
                            <li>Sisteme ne kadar erken katılırsanız, o kadar avantajlı olursunuz.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection