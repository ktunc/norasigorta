@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Tarım Sigortaları</h4>
                        <p>Ekonomiye can veren tarım sektöründe üreticileri desteklemek, geçim kaynaklarında meydana gelecek riskleri güvence altına almak ve tarımsal üretimleri esnasında karşılaşabilecekleri muhtemel zararları telafi etmek amacıyla Devlet Destekli Tarım Sigortaları (TARSİM) ile çiftçilerimizin daima yanında, yakınında bulunmaktadır.</p>

                        <h4>Devlet Destekli Tarım Sigortaları ile</h4>
                        <ul>
                            <li>Açık alanlarda yetiştiriciliği yapılan her türlü bitkisel ürünler,</li>
                            <li>Örtü altı sistemlerinde yetiştirilen ürünler, cam-plastik örtü, teknik donanım ve sera konstrüksiyonu ile yüksek tüneller,</li>
                            <li>Büyükbaş süt veya erkek besi sığırları ile koyun, keçi gibi küçükbaş hayvanlar,</li>
                            <li>Deniz ve iç sularda modern kültür balıkçılığı yöntemleri ile yetiştirilen su ürünleri,</li>
                            <li>Hindi, devekuşu, yumurtalık ve etlik (broiler) tavuklar ile civciv gibi kanatlı kümes hayvanları,</li>
                            <li>Aktif (arılı) ve plakalı arı kovanları</li>
                        </ul>
                        teminat altına alınır.

                        <h4>T.C. Gıda, Tarım ve Hayvancılık Bakanlığı Kayıt Şartı</h4>
                        <p>Devlet destekli tarım sigortalarından yararlanabilmenin temel şartı; ürün, arazi ve hayvan varlıklarına ilişkin işletme kayıtlarının ilgili kayıt sistemlerinde güncel olmasıdır. Çiftçilerimiz Bakanlığın vermiş olduğu sigorta prim desteğinden ve diğer tarımsal desteklerden yararlanmak için ÇKS, TÜRKVET, SKS ve AKS gibi kayıt sistemlerine kayıtlı olmak zorundadır.</p>
                        <ol>
                            <li>Bitkisel Ürünler ve Araziler: Çiftçi Kayıt Sistemi (ÇKS)</li>
                            <li>Cam veya Plastik Seralar: Örtü Altı Kayıt Sistemi (ÇKS)</li>
                            <li>Büyükbaş, Küçükbaş Hayvanlar: Veteriner Bilgi Sistemi (TÜRKVET)</li>
                            <li>Su Ürünleri: Su Ürünleri Kayıt Sistemi (SKS)</li>
                            <li>Arılar ve Kovanları: Arıcılık Kayıt Sistemi (AKS)</li>
                            <li>Kanatlı Kümes Hayvanları: Kapalı Sistem, biyo güvenlik ve hijyen tedbirleri alınmış işletmeler</li>
                        </ol>
                        <p><strong>ÇKS Nedir: Sağlıklı tarım politikalarının oluşturulması, güncellenmesi, geliştirilmesi ve tarımsal desteklemelerin denetlenebilir, izlenebilir bir şekilde yürütülmesini sağlayan TC Gıda Tarım ve Hayvancılık Bakanlığı bünyesinde işlemleri yürütülen bir kayıt sistemidir. Üreticiler, gerek tarım sigortasındaki prim desteğinden, gerekse diğer tarımsal desteklerden yararlanmak için Çiftçi Kayıt Sistemi’ne kayıt yaptırmak ve her sene bilgilerini güncellemek zorundadır.</strong></p>

                        <h4>Poliçe Teslimi, Risk Analiz İşlemleri ve Teminat Süreleri</h4>
                        <p>Açık alanlarda yetiştiriciliği yapılan bitkisel ürüne ve bölgeye göre değişmekle birlikte, TARSİM tarafından bildirilen poliçe kabul dönemlerinde yapılan başvurularda aynı gün poliçe yapılabilmekte ve üreticilerimize teslim edilebilmektedir.</p>

                        <p>Sera, Kümes, Su ürünleri, Arı Kovanı, Büyükbaş ve Küçükbaş Hayvan Hayat sigortaları ile poliçe kabul dönemleri dışında yapılan bitkisel ürün sigorta başvurularında her halükarda işletmede ve üretim alanında risk analizi yapılmakta, risk analiz işlemlerinin tamamlanması sonrası olumlu sonuçlanan başvurular sigorta yapılmaktadır. Risk analiz işlemleri sonucunda, TARSİM tarafından sigortalanması uygun görülmeyen ürün, işletme, tesis ve riskler sigorta edilmez.</p>
                        <p>Bitkisel ürünlerde teminat süresi sonu; ürünün hasat dönemi ile sınırlıdır.<br>
                            Sera, Büyükbaş Süt ve Küçükbaş hayvanlar, Su ürünleri ile Arı Kovanları 1 YIL süreyle,<br>
                            Büyükbaş Besi Hayvanları 3, 6, 9 ve 12 aylık dönemlerde,<br>
                            Kümes Hayvanları ise kanatlı hayvan cinsine ve yetiştiricilik amacına bağlı olarak teminat süresi uygulanır.</p>

                        <h4>Devletin Sigorta Prim Desteği</h4>
                        <p>Tarım sigortalarını geliştirmek amacıyla, üreticiler tarafından ödenmesi gereken sigorta priminin bir kısmı T.C. Gıda, Tarım ve Hayvancılık Bakanlığı tarafından sigortalı adına TARSİM'e ödenmektedir. Ürünler, riskler, bölgeler ve işletme ölçekleri itibariyle Devlet tarafından sağlanacak prim desteği miktarları, her yıl Bakanlığın teklifi üzerine Bakanlar Kurulu tarafından belirlenir.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection