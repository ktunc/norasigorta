@extends('app')

@section('content')
    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>Özel Sağlık Sigortası Nedir?</h4>
                        <p>Beklenmedik bir kaza veya hastalık sonucu oluşacak sağlık giderlerinizin yanı sıra sağlığınız için ihtiyaç duyacağınız her türlü tanı ve tedavi işlemlerini, en seçkin sağlık kurumlarında en modern yöntemlerle ve mali kaygı duymadan karşılanması için hazırlanmış sigortalardır. Sağlık giderleriniz, genel ve özel şartlar dahilinde ve poliçede belirtilen limitler doğrultusunda karşılanır.</p>
                        <p>Özel Sağlık Sigortaları, yatarak tedavi hizmetlerinin yanı sıra acil yardım, aile hekimliği,PSA ve Mamografi , ayakta tedavi, doğum,yeni doğan bebek bakımı ve check-up gibi sağlık hizmetlerini de sunar.</p>

                        <h4>Özel Sağlık Sigortası Primleri Neye Göre Belirlenir?</h4>
                        <p>Sağlık Sigortası primleri poliçe çeşitlerine göre farklılık göstermekle birlikte (yatarak tedavi teminatlı , yatarak + ayakta tedavi teminatlı gibi) sigortalı adayının yaş, cinsiyet , sağlık geçmişi gibi kriterler de poliçe primini belirleyen unsurlar arasında yer alır. Herhangi bir sosyal güvencesi olsun olmasın 18 yaşından büyük olan herkes özel sağlık sigortası yaptırabilir. 18 yaş altı çocuklar ise yanlarında bir ebeveyn ile birlikte poliçe kapsamı içerisine dahil edilir.</p>

                        <h4>Özel Sağlık Sigortası Yaptırırken Dikkat Edilmesi Gereken Konular Nelerdir?</h4>
                        <p>Özel Sağlık Sigortası ile ilgili sigorta sektöründe teminat ve primleri farklı olan çok fazla poliçe çeşidi bulunuyor. Bunlar, primi doğrudan etkileyen teminat limitleri, sigortalı katılım payı oranları, anlaşmalı hastane kısıtlamaları gibi birden fazla unsuru içerisinde barındırır. Bu nedenle poliçenizi yaptırmadan önce ne tür bir sağlık sigortasına ihtiyacınız olduğunu belirleyip buna göre alternatifleri araştırmanızı tavsiye ederiz.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection