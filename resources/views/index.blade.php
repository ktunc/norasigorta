@extends('app')

@section('content')
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{ asset('images/slider/1.jpg') }}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('images/slider/2.jpg') }}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('images/slider/3.jpg') }}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('images/slider/4.jpg') }}" alt="First slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <!-- Hero Slider Area -->
    {{--<div class="heroslider-area in-sliderarrow">--}}

        {{--<!-- Hero Slider Single -->--}}
        {{--<div class="heroslider animated-heroslider d-flex align-items-center" data-bgimage="{{ asset('images/bg/background-image-1.jpg') }}"--}}
             {{--data-secondary-overlay="8">--}}
            {{--<div class="container">--}}
                {{--<div class="row align-items-center">--}}
                    {{--<div class="col-xl-9 col-lg-8">--}}
                        {{--<div class="heroslider-content">--}}
                            {{--<h1><span>For better future </span>Gregory money</h1>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
                                {{--incididunt ut labore et--}}
                                {{--dolore</p>--}}
                            {{--<div class="heroslider-buttonholder">--}}
                                {{--<a href="about-us.html" class="in-button in-button-theme">About Us</a>--}}
                                {{--<a href="contact.html" class="in-button in-button-colorwhite">Contact Us</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-3">--}}
                        {{--<div class="heroslider-roundbox roundbox">--}}
                            {{--<div class="roundbox-block">--}}
									{{--<span class="roundbox-icon">--}}
										{{--<i class="flaticon-mind"></i>--}}
									{{--</span>--}}
                                {{--<h5>Personal PLan</h5>--}}
                            {{--</div>--}}
                            {{--<div class="roundbox-block">--}}
									{{--<span class="roundbox-icon">--}}
										{{--<i class="flaticon-life-insurence"></i>--}}
									{{--</span>--}}
                                {{--<h5>Famliy PLan</h5>--}}
                            {{--</div>--}}
                            {{--<div class="roundbox-block">--}}
									{{--<span class="roundbox-icon">--}}
										{{--<i class="flaticon-planning"></i>--}}
									{{--</span>--}}
                                {{--<h5>Group PLan</h5>--}}
                            {{--</div>--}}
                            {{--<div class="roundbox-block">--}}
									{{--<span class="roundbox-icon">--}}
										{{--<i class="flaticon-businessman"></i>--}}
									{{--</span>--}}
                                {{--<h5>Others PLan</h5>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!--// Hero Slider Single -->--}}

        {{--<!-- Hero Slider Single -->--}}
        {{--<div class="heroslider animated-heroslider d-flex align-items-center" data-bgimage="{{ asset('images/bg/background-image-3.jpg') }}"--}}
             {{--data-secondary-overlay="8">--}}
            {{--<div class="container">--}}
                {{--<div class="row justify-content-center">--}}
                    {{--<div class="col-lg-10">--}}
                        {{--<div class="heroslider-content text-center">--}}
                            {{--<h1><span>For better future </span>Gregory Services</h1>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
                                {{--incididunt ut labore et--}}
                                {{--dolore</p>--}}
                            {{--<div class="heroslider-buttonholder">--}}
                                {{--<a href="about-us.html" class="in-button in-button-theme">About Us</a>--}}
                                {{--<a href="contact.html" class="in-button in-button-colorwhite">Contact Us</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!--// Hero Slider Single -->--}}

        {{--<!-- Hero Slider Single -->--}}
        {{--<div class="heroslider animated-heroslider d-flex align-items-center" data-bgimage="{{ asset('images/bg/background-image-2.jpg') }}"--}}
             {{--data-secondary-overlay="8">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-lg-10">--}}
                        {{--<div class="heroslider-content">--}}
                            {{--<h1><span>For better future </span>Secure Account</h1>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
                                {{--incididunt ut labore et--}}
                                {{--dolore</p>--}}
                            {{--<div class="heroslider-buttonholder">--}}
                                {{--<a href="about-us.html" class="in-button in-button-theme">About Us</a>--}}
                                {{--<a href="contact.html" class="in-button in-button-colorwhite">Contact Us</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!--// Hero Slider Single -->--}}

    {{--</div>--}}
    <!--// Hero Slider Area -->

    <!-- Features Area -->
    <div class="features-area in-section section-padding-md bg-white">
        <div class="container">
            <div class="row features-wrapper">
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="in-feature">
								<span class="in-feature-icon" style="line-height: 20px">
									<i class="flaticon-lock"></i>
								</span>
                        <h4>Güvenilir</h4>
                        {{--<p>Do eiusmod tempor incididunt ut labore et dolore masit amet.</p>--}}
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="in-feature">
								<span class="in-feature-icon" style="line-height: 20px">
									<i class="flaticon-lab"></i>
								</span>
                        <h4>10 Yıllık Tecrübe</h4>
                        {{--<p>Do eiusmod tempor incididunt ut labore et dolore masit amet.</p>--}}
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="in-feature">
								<span class="in-feature-icon" style="line-height: 20px">
									<i class="flaticon-partner"></i>
								</span>
                        <h4>Güçlü Ekip</h4>
                        {{--<p>Do eiusmod tempor incididunt ut labore et dolore masit amet.</p>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// Features Area -->

    <!-- Services Area -->
    <div class="services-area in-section section-padding-lg bg-shape">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center">
                        {{--<h6>BEST SERVICES FOR YOU</h6>--}}
                        <h2>Teklif Alın</h2>
                    </div>
                </div>
            </div>
            <div class="row">

                <!-- Single Service -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="in-service mt-30">
								<span class="in-service-icon">
									<i class="flaticon-car-1"></i>
								</span>
                        <h5><a href="javascript:void(0)" onclick="FuncSigortaTeklifi('trafik','Trafik Sigortası Teklifi Alın')">Trafik Sigortası</a></h5>
                        {{--<p>adipconsequat. Duis aute irure dolor in repreheq.</p>--}}
                        <span class="in-service-transparenticon">
									<i class="flaticon-car-1"></i>
								</span>
                    </div>
                </div>
                <!--// Single Service -->

                <!-- Single Service -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="in-service mt-30">
								<span class="in-service-icon">
									<i class="flaticon-car"></i>
								</span>
                        <h5><a href="javascript:void(0)" onclick="FuncSigortaTeklifi('kasko','Kasko Teklifi Alın')">Kasko</a></h5>
                        {{--<p>adipconsequat. Duis aute irure dolor in repreheq.</p>--}}
                        <span class="in-service-transparenticon">
									<i class="flaticon-car"></i>
								</span>
                    </div>
                </div>
                <!--// Single Service -->

                <!-- Single Service -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="in-service mt-30">
								<span class="in-service-icon">
									<i class="flaticon-health-insurance"></i>
								</span>
                        <h5><a href="javascript:void(0)" onclick="FuncSigortaTeklifi('saglik','Sağlık Sigortası Teklifi Alın')">Sağlık Sigortası</a></h5>
                        {{--<p>adipconsequat. Duis aute irure dolor in repreheq.</p>--}}
                        <span class="in-service-transparenticon">
									<i class="flaticon-health-insurance"></i>
								</span>
                    </div>
                </div>
                <!--// Single Service -->

                <!-- Single Service -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="in-service mt-30">
								<span class="in-service-icon">
									<i class="flaticon-mortgage"></i>
								</span>
                        <h5><a href="javascript:void(0)" onclick="FuncSigortaTeklifi('konut','Konut Sigortası Teklifi Alın')">Konut Sigortası</a></h5>
                        {{--<p>adipconsequat. Duis aute irure dolor in repreheq.</p>--}}
                        <span class="in-service-transparenticon">
									<i class="flaticon-mortgage"></i>
								</span>
                    </div>
                </div>
                <!--// Single Service -->

                <!-- Single Service -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="in-service mt-30">
								<span class="in-service-icon">
									<i class="flaticon-van"></i>
								</span>
                        <h5><a href="javascript:void(0)">Seyahat Sigortası</a></h5>
                        {{--<p>adipconsequat. Duis aute irure dolor in repreheq.</p>--}}
                        <span class="in-service-transparenticon">
									<i class="flaticon-van"></i>
								</span>
                    </div>
                </div>
                <!--// Single Service -->

                <!-- Single Service -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="in-service mt-30">
								<span class="in-service-icon">
									<i class="flaticon-bars"></i>
								</span>
                        <h5><a href="javascript:void(0)">İşyeri Sigortası</a></h5>
                        {{--<p>adipconsequat. Duis aute irure dolor in repreheq.</p>--}}
                        <span class="in-service-transparenticon">
									<i class="flaticon-bars"></i>
								</span>
                    </div>
                </div>
                <!--// Single Service -->

                {{--<!-- Single Service -->--}}
                {{--<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">--}}
                    {{--<div class="in-service mt-30">--}}
								{{--<span class="in-service-icon">--}}
									{{--<i class="flaticon-life-insurence"></i>--}}
								{{--</span>--}}
                        {{--<h5><a href="services-details.html">Life Insurance</a></h5>--}}
                        {{--<p>adipconsequat. Duis aute irure dolor in repreheq.</p>--}}
                        {{--<span class="in-service-transparenticon">--}}
									{{--<i class="flaticon-life-insurence"></i>--}}
								{{--</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!--// Single Service -->--}}

                {{--<!-- Single Service -->--}}
                {{--<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">--}}
                    {{--<div class="in-service mt-30">--}}
								{{--<span class="in-service-icon">--}}
									{{--<i class="flaticon-agriculture"></i>--}}
								{{--</span>--}}
                        {{--<h5><a href="services-details.html">Agricultural Insurance</a></h5>--}}
                        {{--<p>adipconsequat. Duis aute irure dolor in repreheq.</p>--}}
                        {{--<span class="in-service-transparenticon">--}}
									{{--<i class="flaticon-agriculture"></i>--}}
								{{--</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!--// Single Service -->--}}

            </div>
        </div>
    </div>
    <!--// Services Area -->

    <!-- About Area -->
    {{--<div class="about-area in-section section-padding-top-lg bg-white">--}}
        {{--<div class="container custom-container">--}}
            {{--<div class="row no-gutters">--}}
                {{--<div class="col-xl-6 col-lg-12">--}}
                    {{--<div class="about-content heightmatch">--}}
                        {{--<h6>Biz Kimiz </h6>--}}
                        {{--<h2>Biz Kimiz</h2>--}}
                        {{--<h4>Mutlu yaşamınızı sürdürmek için uluslararası sigorta servis sağlayıcısına liderlik ediyoruz.</h4>--}}
                        {{--<ul class="ul-style-1">--}}
                            {{--<li>Lorem ipsum dolor sit amet, ciit in voluptate velit esse cillum.</li>--}}
                            {{--<li>Tempor at. Duis aute irure dolor in reprehenderit in voluptate veldolou fugiat nulla paria turat.</li>--}}
                            {{--<li>Omnis iste natus error sit voluptatem accusantium doloremque laudantiuquae ab illo inventore veritatis et--}}
                                {{--quasi dolorem.</li>--}}
                            {{--<li>Do eiusmod tempor incididunt ut labore et dolore masit amet.</li>--}}
                        {{--</ul>--}}
                        {{--<a href="{{url('/hakkimizda')}}" class="in-button">Hakkımızda</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-xl-6 col-lg-6">--}}
                    {{--<div class="heightmatch">--}}
                        {{--<div class="in-videobox">--}}
                            {{--<img src="{{ asset('images/other/videbox-image-1.jpg') }}" alt="man with umbrella">--}}
                            {{--<a href="#" data-video-id='136709781' data-channel="vimeo" class="in-videobutton in-videobox-button"><i class="zmdi zmdi-play"></i></a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-xl-6 col-lg-6">--}}
                    {{--<div class="counterbox heightmatch" data-bgimage="{{ asset('images/other/counterbox-bg.jpg') }}" data-secondary-overlay="9">--}}
                        {{--<div class="counterbox-inner">--}}
                            {{--<div class="counterbox-block">--}}
                                {{--<div class="counterbox-blockinner">--}}
                                    {{--<h2><span class="counter">95</span>%</h2>--}}
                                    {{--<h6>Memnuniyet</h6>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="counterbox-block">--}}
                                {{--<div class="counterbox-blockinner">--}}
                                    {{--<h2><span class="counter">4000</span></h2>--}}
                                    {{--<h6>Müşteri</h6>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="counterbox-block">--}}
                                {{--<div class="counterbox-blockinner">--}}
                                    {{--<h2><span class="counter">95</span>%</h2>--}}
                                    {{--<h6>Memnuniyet</h6>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="counterbox-block">--}}
                                {{--<div class="counterbox-blockinner">--}}
                                    {{--<h2><span class="counter">980</span></h2>--}}
                                    {{--<h6>Proje</h6>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-xl-6 col-lg-12">--}}
                    {{--<div class="insurencebox heightmatch">--}}
                        {{--<h4>Request a free call back</h4>--}}
                        {{--<h2>Have a business to protect?</h2>--}}
                        {{--<form action="#" class="insurencebox-form">--}}
                            {{--<select>--}}
                                {{--<option value="life">Life Insurence</option>--}}
                                {{--<option value="home">Home Insurence</option>--}}
                                {{--<option value="travel">Travel Insurence</option>--}}
                                {{--<option value="business">Business Insurence</option>--}}
                                {{--<option value="car">Car Insurence</option>--}}
                                {{--<option value="auto">Auto Insurence</option>--}}
                            {{--</select>--}}
                            {{--<input type="text" placeholder="Name *">--}}
                            {{--<input type="text" placeholder="Email *">--}}
                            {{--<input type="text" placeholder="Phone *">--}}
                            {{--<button type="submit" class="in-button">Submit</button>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <!--// About Area -->

    <!-- Testimonial Area -->
    {{--<div class="testimonial-area in-section section-padding-lg bg-white">--}}

        {{--<div class="testimonial-slider in-slidearrow-2">--}}
            {{--<div class="slider-item">--}}
                {{--<!-- Single Testimonial -->--}}
                {{--<div class="testimonial">--}}
                    {{--<div class="testimonial-header">--}}
                        {{--<div class="testimonial-image">--}}
                            {{--<img src="{{ asset('images/logo/logo.png') }}" alt="testimonial author">--}}
                        {{--</div>--}}
                        {{--<span><img src="{{ asset('images/icons/title-bottom-shape.png') }}" alt="shape"></span>--}}
                    {{--</div>--}}
                    {{--<div class="testimonial-content">--}}
                        {{--<p>Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure abore et dolore magna--}}
                            {{--aliqua. Ut enim ad minim veniam, quis nostrud dolor in reprehenderit </p>--}}
                        {{--<h5>Levent Murat</h5>--}}
                        {{--<h6>Consultant</h6>--}}
                    {{--</div>--}}
                    {{--<ul class="testimonial-socialicons">--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-pinterest"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<!--// Single Testimonial -->--}}
            {{--</div>--}}

            {{--<div class="slider-item">--}}
                {{--<!-- Single Testimonial -->--}}
                {{--<div class="testimonial">--}}
                    {{--<div class="testimonial-header">--}}
                        {{--<div class="testimonial-image">--}}
                            {{--<img src="{{ asset('images/testimonial/testimonial-author-1.png') }}" alt="testimonial author">--}}
                        {{--</div>--}}
                        {{--<span><img src="{{ asset('images/icons/title-bottom-shape.png') }}" alt="shape"></span>--}}
                    {{--</div>--}}
                    {{--<div class="testimonial-content">--}}
                        {{--<p>Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure abore et dolore magna--}}
                            {{--aliqua. Ut enim ad minim veniam, quis nostrud dolor in reprehenderit </p>--}}
                        {{--<h5>Lincoln Schiller</h5>--}}
                        {{--<h6>Consultant</h6>--}}
                    {{--</div>--}}
                    {{--<ul class="testimonial-socialicons">--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-pinterest"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<!--// Single Testimonial -->--}}
            {{--</div>--}}

            {{--<div class="slider-item">--}}
                {{--<!-- Single Testimonial -->--}}
                {{--<div class="testimonial">--}}
                    {{--<div class="testimonial-header">--}}
                        {{--<div class="testimonial-image">--}}
                            {{--<img src="{{ asset('images/testimonial/testimonial-author-1.png') }}" alt="testimonial author">--}}
                        {{--</div>--}}
                        {{--<span><img src="{{ asset('images/icons/title-bottom-shape.png') }}" alt="shape"></span>--}}
                    {{--</div>--}}
                    {{--<div class="testimonial-content">--}}
                        {{--<p>Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure abore et dolore magna--}}
                            {{--aliqua. Ut enim ad minim veniam, quis nostrud dolor in reprehenderit </p>--}}
                        {{--<h5>Theron Gottlieb</h5>--}}
                        {{--<h6>Consultant</h6>--}}
                    {{--</div>--}}
                    {{--<ul class="testimonial-socialicons">--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-pinterest"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<!--// Single Testimonial -->--}}
            {{--</div>--}}

            {{--<div class="slider-item">--}}
                {{--<!-- Single Testimonial -->--}}
                {{--<div class="testimonial">--}}
                    {{--<div class="testimonial-header">--}}
                        {{--<div class="testimonial-image">--}}
                            {{--<img src="{{ asset('images/testimonial/testimonial-author-1.png') }}" alt="testimonial author">--}}
                        {{--</div>--}}
                        {{--<span><img src="{{ asset('images/icons/title-bottom-shape.png') }}" alt="shape"></span>--}}
                    {{--</div>--}}
                    {{--<div class="testimonial-content">--}}
                        {{--<p>Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure abore et dolore magna--}}
                            {{--aliqua. Ut enim ad minim veniam, quis nostrud dolor in reprehenderit </p>--}}
                        {{--<h5>Alejandra Klein</h5>--}}
                        {{--<h6>Consultant</h6>--}}
                    {{--</div>--}}
                    {{--<ul class="testimonial-socialicons">--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-pinterest"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<!--// Single Testimonial -->--}}
            {{--</div>--}}

            {{--<div class="slider-item">--}}
                {{--<!-- Single Testimonial -->--}}
                {{--<div class="testimonial">--}}
                    {{--<div class="testimonial-header">--}}
                        {{--<div class="testimonial-image">--}}
                            {{--<img src="{{ asset('images/testimonial/testimonial-author-1.png') }}" alt="testimonial author">--}}
                        {{--</div>--}}
                        {{--<span><img src="{{ asset('images/icons/title-bottom-shape.png') }}" alt="shape"></span>--}}
                    {{--</div>--}}
                    {{--<div class="testimonial-content">--}}
                        {{--<p>Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure abore et dolore magna--}}
                            {{--aliqua. Ut enim ad minim veniam, quis nostrud dolor in reprehenderit </p>--}}
                        {{--<h5>Bulah Macejkovic</h5>--}}
                        {{--<h6>Consultant</h6>--}}
                    {{--</div>--}}
                    {{--<ul class="testimonial-socialicons">--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-pinterest"></i></a></li>--}}
                        {{--<li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<!--// Single Testimonial -->--}}
            {{--</div>--}}

        {{--</div>--}}
    {{--</div>--}}
    <!--// Testimonial Area -->

    <!-- Call To Action Area -->
    {{--<div class="calltoaction-area in-section section-padding-lg bg-image-1" data-secondary-overlay="9">--}}
        {{--<div class="container">--}}
            {{--<div class="row justify-content-center">--}}
                {{--<div class="col-lg-8">--}}
                    {{--<div class="in-cta text-center">--}}
                        {{--<h4>NEED AN AGENT</h4>--}}
                        {{--<h2>FOR FUTURE PLAN</h2>--}}
                        {{--<p>Lorem ipsum dolor sit amet, con sectetur adipisicing elit, sed do Lorem ipra incidunt ut labore et dolore--}}
                            {{--magnam aliquam quaerat voluptatem.</p>--}}
                        {{--<a href="contact.html" class="in-button in-button-white">Contact Us</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <!--// Call To Action Area -->

    <!-- Blog Area -->
    <div class="blogs-area in-section section-padding-lg bg-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center">
                        {{--<h6>BEST SERVICES FOR YOU</h6>--}}
                        <h2>Bizden Haberler</h2>
                    </div>
                </div>
            </div>
            <div class="row blog-slider-active in-slidearrow">
                @foreach($haberler as $haber)
                    <!-- Single Blog -->
                        <div class="col-12">
                            <div class="in-blog mt-30">
                                <div class="in-blog-image">
                                    <a href="{{url('/haber/'.$haber->id)}}">
                                        @if(count($haber->haberresimleri) > 0)
                                            <img src="{{ asset('storage/'.$haber->haberresimleri[0]->path) }}" alt="blog image">
                                        @else
                                            <img src="{{ asset('images/logo/logo.png') }}" alt="blog image">
                                        @endif
                                    </a>
                                </div>
                                <div class="in-blog-content">
                                    <div class="in-blog-metatop">
                                        <span>{{date_format($haber->created_at, 'd.m.Y H:i:s')}}</span>
                                        {{--<span><a href="#">Life Insurance</a></span>--}}
                                    </div>
                                    <h4 class="in-blog-title"><a href="{{url('/haber/'.$haber->id)}}">{{$haber->baslik}}</a></h4>
                                    <p>{{\Illuminate\Support\Str::limit(strip_tags($haber->icerik))}}</p>
                                    {{--<div class="in-blog-authorimage">--}}
										{{--<span>--}}
											{{--<img src="{{ asset('images/blog/author-image/author-image-2.png') }}" alt="author image">--}}
										{{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="in-blog-metabottom">--}}
                                        {{--<span>By <a href="#">Admin</a></span>--}}
                                        {{--<span><a href="#"><i class="zmdi zmdi-favorite-outline"></i> Like : 05</a> / <a href="#"><i class="zmdi zmdi-comment-outline"></i>--}}
												{{--Comment</a></span>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                        </div>
                        <!--// Single Blog -->
                @endforeach



                <!-- Single Blog -->
                {{--<div class="col-lg-12">--}}
                    {{--<div class="in-blog mt-30">--}}
                        {{--<div class="in-blog-image">--}}
                            {{--<a href="#">--}}
                                {{--<img src="{{ asset('images/blog/blog-image-2.jpg') }}" alt="blog image">--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        {{--<div class="in-blog-content">--}}
                            {{--<div class="in-blog-metatop">--}}
                                {{--<span>15th sep, 2018</span>--}}
                                {{--<span><a href="#">Life Insurance</a></span>--}}
                            {{--</div>--}}
                            {{--<h4 class="in-blog-title"><a href="#">We are leading insurance--}}
                                    {{--service.</a></h4>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consecte adipicing elit, sed do eiusmod tempor </p>--}}
                            {{--<div class="in-blog-authorimage">--}}
										{{--<span>--}}
											{{--<img src="{{ asset('images/blog/author-image/author-image-2.png') }}" alt="author image">--}}
										{{--</span>--}}
                            {{--</div>--}}
                            {{--<div class="in-blog-metabottom">--}}
                                {{--<span>By <a href="#">Admin</a></span>--}}
                                {{--<span><a href="#"><i class="zmdi zmdi-favorite-outline"></i> Like : 05</a> / <a href="#"><i class="zmdi zmdi-comment-outline"></i>--}}
												{{--Comment</a></span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <!--// Single Blog -->

            </div>
        </div>
    </div>
    <!--// Blog Area -->

<script type="text/javascript">
var jcTeklif;
$(document).ready(function () {
    $(document).on('submit','form#FormTeklifAl',function (event) {
        event.preventDefault();
        var formdata = new FormData($('form#FormTeklifAl').get(0));
        $.ajax({
            type:'POST',
            url:'{{url('/teklifal')}}',
            data:formdata,
            processData: false,
            contentType: false,
            dataType:'json',
            beforeSend:function () {
                jcTeklif.showLoading(true);
            }
        }).done(function (data) {
            if(data.hata){
                $.alert({
                    theme:'modern',
                    type:'red',
                    icon:'fa fa-close',
                    title:'Hata',
                    content:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                    onClose:function () {
                        jcTeklif.hideLoading();
                    }
                });
            }else{
                $.alert({
                    theme:'modern',
                    type:'green',
                    icon:'fa fa-check',
                    title:'Başarılı',
                    content:'Teklif talebi başarıyal gönderildi.',
                    onClose:function () {
                        jcTeklif.close();
                    }
                });
            }
        }).fail(function () {
            $.alert({
                theme:'modern',
                type:'red',
                icon:'fa fa-close',
                title:'Hata',
                content:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                onClose:function () {
                    jcTeklif.hideLoading();
                }
            });
        });
    })
});

var sahis =  '  <div class="form-group row">\n' +
    '    <label for="tc" class="col-sm-2 col-form-label">T.C. Kimlik No:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '      <input type="number" class="form-control" name="tc" id="tc" required>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '  <div class="form-group row">\n' +
    '    <label for="adsoyad" class="col-sm-2 col-form-label">Adınız Soyadınız:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '      <input type="text" class="form-control" name="adsoyad" id="adsoyad" required>\n' +
    '    </div>\n' +
    '  </div>\n';

var sirket = '  <div class="form-group row">\n' +
    '    <label for="vergino" class="col-sm-2 col-form-label">Vergi Kimlik No:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '      <input type="number" class="form-control" name="vergino" id="vergino" required>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '  <div class="form-group row">\n' +
    '    <label for="sirketadi" class="col-sm-2 col-form-label">Şirket Adı:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '      <input type="text" class="form-control" name="sirketadi" id="sirketadi" required>\n' +
    '    </div>\n' +
    '  </div>\n';

var yenileme = '<div class="form-group row">\n' +
    '<label for="belgeno" class="col-sm-2 col-form-label">Belge Seri No:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="text" class="form-control" name="belgeno" id="belgeno">\n' +
    '</div>\n' +
    '</div>\n';

var yenikayit = '<div class="form-group row">\n' +
    '<label for="asbis" class="col-sm-2 col-form-label">ASBİS No:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="text" class="form-control" name="asbis" id="asbis">\n' +
    '</div>\n' +
    '</div>\n';

var sigortateklifi = {
    trafik:'<div class="container">\n' +
    '<form id="FormTeklifAl">\n' +
    '<div class="form-group row">\n' +
        '<div class="col-sm-12 text-center">\n' +
            '<h5>Araç Sahibi Bilgileri</h5>\n' +
        '</div>\n' +
    '</div>\n' +
    '  <div class="form-group row">\n' +
    '    <label for="ruhsatsahibi" class="col-sm-2 col-form-label">Ruhsat Sahibi:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '<label>\n' +
    '<input type="radio" name="ruhsat" value="sahis" checked="checked">\n' +
    'Şahıs\n' +
    '</label>\n' +
    '<label>\n' +
    '<input type="radio" name="ruhsat" value="sirket">\n' +
    'Şirket\n' +
    '</label>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '  <div id="sahissirket">\n' +
    '  <div class="form-group row">\n' +
    '    <label for="tc" class="col-sm-2 col-form-label">T.C. Kimlik No:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '      <input type="number" class="form-control" name="tc" id="tc" required>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '  <div class="form-group row">\n' +
    '    <label for="adsoyad" class="col-sm-2 col-form-label">Adınız Soyadınız:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '      <input type="text" class="form-control" name="adsoyad" id="adsoyad" required>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '  </div>\n' +
    
    '<div class="form-group row">\n' +
    '<label for="il" class="col-sm-2 col-form-label">Şehir:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<select class="form-control" name="il" id="il" required>\n' +
    '<option value="0">Seçiniz</option>\n' +
    '<option value="ADANA">ADANA</option><option value="ADIYAMAN">ADIYAMAN</option><option value="AFYON">AFYON</option><option value="AĞRI">AĞRI</option><option value="AKSARAY">AKSARAY</option><option value="AMASYA">AMASYA</option><option value="ANKARA">ANKARA</option><option value="ANTALYA">ANTALYA</option><option value="ARDAHAN">ARDAHAN</option><option value="ARTVİN">ARTVİN</option><option value="AYDIN">AYDIN</option><option value="BALIKESİR">BALIKESİR</option><option value="BARTIN">BARTIN</option><option value="BATMAN">BATMAN</option><option value="BAYBURT">BAYBURT</option><option value="BİLECİK">BİLECİK</option><option value="BİNGÖL">BİNGÖL</option><option value="BİTLİS">BİTLİS</option><option value="BOLU">BOLU</option><option value="BURDUR">BURDUR</option><option value="BURSA">BURSA</option><option value="ÇANAKKALE">ÇANAKKALE</option><option value="ÇANKIRI">ÇANKIRI</option><option value="ÇORUM">ÇORUM</option><option value="DENİZLİ">DENİZLİ</option><option value="DİYARBAKIR">DİYARBAKIR</option><option value="DÜZCE">DÜZCE</option><option value="EDİRNE">EDİRNE</option><option value="ELAZIĞ">ELAZIĞ</option><option value="ERZİNCAN">ERZİNCAN</option><option value="ERZURUM">ERZURUM</option><option value="ESKİŞEHİR">ESKİŞEHİR</option><option value="GAZİANTEP">GAZİANTEP</option><option value="GİRESUN">GİRESUN</option><option value="GÜMÜŞHANE">GÜMÜŞHANE</option><option value="HAKKARİ">HAKKARİ</option><option value="HATAY">HATAY</option><option value="İÇEL">İÇEL</option><option value="IĞDIR">IĞDIR</option><option value="ISPARTA">ISPARTA</option><option value="İSTANBUL">İSTANBUL</option><option value="İZMİR">İZMİR</option><option value="KAHRAMANMARAŞ">KAHRAMANMARAŞ</option><option value="KARABÜK">KARABÜK</option><option value="KARAMAN">KARAMAN</option><option value="KARS">KARS</option><option value="KASTAMONU">KASTAMONU</option><option value="KAYSERİ">KAYSERİ</option><option value="KİLİS">KİLİS</option><option value="KIRIKKALE">KIRIKKALE</option><option value="KIRKLARELİ">KIRKLARELİ</option><option value="KIRŞEHİR">KIRŞEHİR</option><option value="KOCAELİ">KOCAELİ</option><option value="KONYA">KONYA</option><option value="KÜTAHYA">KÜTAHYA</option><option value="MALATYA">MALATYA</option><option value="MANİSA">MANİSA</option><option value="MARDİN">MARDİN</option><option value="MUĞLA">MUĞLA</option><option value="MUŞ">MUŞ</option><option value="NEVŞEHİR">NEVŞEHİR</option><option value="NİĞDE">NİĞDE</option><option value="ORDU">ORDU</option><option value="OSMANİYE">OSMANİYE</option><option value="RİZE">RİZE</option><option value="SAKARYA">SAKARYA</option><option value="SAMSUN">SAMSUN</option><option value="ŞANLIURFA">ŞANLIURFA</option><option value="SİİRT">SİİRT</option><option value="SİNOP">SİNOP</option><option value="ŞIRNAK">ŞIRNAK</option><option value="SİVAS">SİVAS</option><option value="TEKİRDAĞ">TEKİRDAĞ</option><option value="TOKAT">TOKAT</option><option value="TRABZON">TRABZON</option><option value="TUNCELİ">TUNCELİ</option><option value="UŞAK">UŞAK</option><option value="VAN">VAN</option><option value="YALOVA">YALOVA</option><option value="YOZGAT">YOZGAT</option><option value="ZONGULDAK">ZONGULDAK</option>                        </select>\n' +
    '</div>\n' +
    '</div>\n' +
    
    '<div class="form-group row">\n' +
    '<label for="tel" class="col-sm-2 col-form-label">Telefon:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="number" class="form-control" name="tel" id="tel" required>\n' +
    '</div>\n' +
    '</div>\n' +
    
    '<div class="form-group row">\n' +
    '<label for="mail" class="col-sm-2 col-form-label">Mail :</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="email" class="form-control" name="mail" id="mail" required>\n' +
    '</div>\n' +
    '</div>\n' +
    
    '<div class="form-group row">\n' +
    '<hr>\n' +
    '</div>\n' +


    '<div class="form-group row">\n' +
        '<div class="col-sm-12 text-center">\n' +
            '<h5>Araç Bilgileri</h5>\n' +
        '</div>\n' +
    '</div>\n' +
    
    '<div class="form-group row">\n' +
    '    <label for="ruhsatislemturu" class="col-sm-2 col-form-label">İşlem Türü:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '<label>\n' +
    '<input type="radio" name="ruhsatislemturu" value="yenileme" checked="checked">\n' +
    'Yenileme\n' +
    '</label>\n' +
    '<label>\n' +
    '<input type="radio" name="ruhsatislemturu" value="yenikayit">\n' +
    'Yeni Kayıt\n' +
    '</label>\n' +
    '    </div>\n' +
    '  </div>\n' +
    
    '<div id="yenilemeyenikayit">\n' +
    '<div class="form-group row">\n' +
    '<label for="belgeno" class="col-sm-2 col-form-label">Belge Seri No:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="text" class="form-control" name="belgeno" id="belgeno">\n' +
    '</div>\n' +
    '</div>\n' +
    '</div>\n' +

    '<div class="form-group row">\n' +
    '<label for="marka" class="col-sm-2 col-form-label">Marka:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<select class="form-control" name="marka" id="marka" required>\n' +
    '<option value="0">Seçiniz</option><option value="AIXAM2">AIXAM2</option><option value="AKIA">AKIA</option><option value="ALFA ROMEO">ALFA ROMEO</option><option value="AR-BUS">AR-BUS</option><option value="ASKAM/FARGO/DESOTO">ASKAM/FARGO/DESOTO</option><option value="ASTON MARTIN">ASTON MARTIN</option><option value="ASTRA">ASTRA</option><option value="AUDI">AUDI</option><option value="AVIA">AVIA</option><option value="BENTLEY">BENTLEY</option><option value="BMC">BMC</option><option value="BMW">BMW</option><option value="BREDAMENARIBUS">BREDAMENARIBUS</option><option value="BUGATTI">BUGATTI</option><option value="BUICK">BUICK</option><option value="BUSCLUB">BUSCLUB</option><option value="CADILLAC">CADILLAC</option><option value="CATERHAM">CATERHAM</option><option value="CHANGAN">CHANGAN</option><option value="CHERY">CHERY</option><option value="CHEVROLET">CHEVROLET</option><option value="CHRYSLER">CHRYSLER</option><option value="CITROEN">CITROEN</option><option value="DACIA">DACIA</option><option value="DAEWOO">DAEWOO</option><option value="DAF">DAF</option><option value="DAIHATSU">DAIHATSU</option><option value="DFM">DFM</option><option value="DODGE/USA">DODGE/USA</option><option value="EICHER">EICHER</option><option value="FAW">FAW</option><option value="FERRARI">FERRARI</option><option value="FIAT">FIAT</option><option value="FISKER">FISKER</option><option value="FOLKVAN">FOLKVAN</option><option value="FORD/OTOSAN">FORD/OTOSAN</option><option value="FORD/USA">FORD/USA</option><option value="GAZ">GAZ</option><option value="GEELY">GEELY</option><option value="GMC">GMC</option><option value="GONOW">GONOW</option><option value="GREAT WALL">GREAT WALL</option><option value="GROVE">GROVE</option><option value="GULERYUZ">GULERYUZ</option><option value="HASKAR">HASKAR</option><option value="HINO">HINO</option><option value="HISCAR">HISCAR</option><option value="HONDA">HONDA</option><option value="HUANGHAI">HUANGHAI</option><option value="HUMMER">HUMMER</option><option value="HYMER">HYMER</option><option value="HYUNDAI">HYUNDAI</option><option value="IKCO">IKCO</option><option value="INFINITI">INFINITI</option><option value="IRIZAR">IRIZAR</option><option value="ISOBUS">ISOBUS</option><option value="ISUZU">ISUZU</option><option value="JAC">JAC</option><option value="JAGUAR">JAGUAR</option><option value="KANUNI">KANUNI</option><option value="KARSAN">KARSAN</option><option value="KENWORTH">KENWORTH</option><option value="KIA">KIA</option><option value="KOENIGSEGG">KOENIGSEGG</option><option value="KTM">KTM</option><option value="LADA">LADA</option><option value="LAMBORGHINI">LAMBORGHINI</option><option value="LANCIA">LANCIA</option><option value="LAND ROVER">LAND ROVER</option><option value="LINCOLN">LINCOLN</option><option value="LOTUS">LOTUS</option><option value="MAHINDRA">MAHINDRA</option><option value="MAN">MAN</option><option value="MASERATI">MASERATI</option><option value="MAYBACH">MAYBACH</option><option value="MAZDA">MAZDA</option><option value="MEGA">MEGA</option><option value="MERCEDES">MERCEDES</option><option value="MERCURY">MERCURY</option><option value="MICROCAR">MICROCAR</option><option value="MINI">MINI</option><option value="MITSUBISHI">MITSUBISHI</option><option value="MJT">MJT</option><option value="MOTORSIKLET">MOTORSIKLET</option><option value="NEOPLAN">NEOPLAN</option><option value="NISSAN">NISSAN</option><option value="OPEL">OPEL</option><option value="OTOKAR/MAGIRUS">OTOKAR/MAGIRUS</option><option value="OTOYOL/IVECO/FIAT">OTOYOL/IVECO/FIAT</option><option value="PETERBILT">PETERBILT</option><option value="PEUGEOT">PEUGEOT</option><option value="PGO">PGO</option><option value="PIAGGIO">PIAGGIO</option><option value="PONTIAC">PONTIAC</option><option value="PORSCHE">PORSCHE</option><option value="PROTON">PROTON</option><option value="RANGE ROVER">RANGE ROVER</option><option value="RENAULT">RENAULT</option><option value="RENAULT (OYAK)">RENAULT (OYAK)</option><option value="ROLLS-ROYCE">ROLLS-ROYCE</option><option value="ROVER">ROVER</option><option value="SAAB">SAAB</option><option value="SAMSUNG">SAMSUNG</option><option value="SCAM">SCAM</option><option value="SCANIA">SCANIA</option><option value="SEAT">SEAT</option><option value="SETRA">SETRA</option><option value="SHUANGHUAN">SHUANGHUAN</option><option value="SINOTRUK">SINOTRUK</option><option value="SKODA">SKODA</option><option value="SMART">SMART</option><option value="SSANGYONG">SSANGYONG</option><option value="SUBARU">SUBARU</option><option value="SUZUKI">SUZUKI</option><option value="TATA">TATA</option><option value="TCV">TCV</option><option value="TEMSA">TEMSA</option><option value="TESLA">TESLA</option><option value="TEZELLER">TEZELLER</option><option value="THE LONDON TAXI">THE LONDON TAXI</option><option value="TOFAS-FIAT">TOFAS-FIAT</option><option value="TOYOTA">TOYOTA</option><option value="TURKAR">TURKAR</option><option value="TURKKAR">TURKKAR</option><option value="VANHOLL">VANHOLL</option><option value="VEICOLI">VEICOLI</option><option value="VISEON">VISEON</option><option value="VOLKSWAGEN">VOLKSWAGEN</option><option value="VOLVO">VOLVO</option><option value="VOLVO-TR">VOLVO-TR</option><option value="WIESMANN">WIESMANN</option><option value="ZIRAI TRAKTOR">ZIRAI TRAKTOR</option><option value="ZONDA">ZONDA</option>\n' +
    '</select>\n' +
    '</div>\n' +
    '</div>\n' +
    
    '<div class="form-group row">\n' +
    '<label for="model" class="col-sm-2 col-form-label">Model:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="text" class="form-control" name="model" id="model" required>\n' +
    '</div>\n' +
    '</div>\n' +
    
    '<div class="form-group row">\n' +
    '<label for="modelyili" class="col-sm-2 col-form-label">Model Yılı:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="text" class="form-control" name="modelyili" id="modelyili" required>\n' +
    '</div>\n' +
    '</div>\n' +
    
    '<div class="form-group row">\n' +
    '<label for="plaka" class="col-sm-2 col-form-label">Plaka:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="text" class="form-control" name="plaka" id="plaka" required>\n' +
    '</div>\n' +
    '</div>\n' +
    
    '<div class="form-group row">\n' +
    '<div class="col-sm-12 text-center">\n' +
        '<button type="submit" class="in-button">TEKLİF AL</button>\n' +
    '</div>\n' +
    '</div>\n' +
    
    '</form>\n' +
    '</div>',
    kasko:'<div class="container">\n' +
    '<form id="FormTeklifAl">\n' +
    '<div class="form-group row">\n' +
    '<div class="col-sm-12 text-center">\n' +
    '<h5>Araç Sahibi Bilgileri</h5>\n' +
    '</div>\n' +
    '</div>\n' +
    '  <div class="form-group row">\n' +
    '    <label for="ruhsatsahibi" class="col-sm-2 col-form-label">Ruhsat Sahibi:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '<label>\n' +
    '<input type="radio" name="ruhsat" value="sahis" checked="checked">\n' +
    'Şahıs\n' +
    '</label>\n' +
    '<label>\n' +
    '<input type="radio" name="ruhsat" value="sirket">\n' +
    'Şirket\n' +
    '</label>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '  <div id="sahissirket">\n' +
    '  <div class="form-group row">\n' +
    '    <label for="tc" class="col-sm-2 col-form-label">T.C. Kimlik No:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '      <input type="number" class="form-control" name="tc" id="tc" required>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '  <div class="form-group row">\n' +
    '    <label for="adsoyad" class="col-sm-2 col-form-label">Adınız Soyadınız:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '      <input type="text" class="form-control" name="adsoyad" id="adsoyad" required>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '  </div>\n' +

    '<div class="form-group row">\n' +
    '<label for="il" class="col-sm-2 col-form-label">Şehir:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<select class="form-control" name="il" id="il" required>\n' +
    '<option value="0">Seçiniz</option>\n' +
    '<option value="ADANA">ADANA</option><option value="ADIYAMAN">ADIYAMAN</option><option value="AFYON">AFYON</option><option value="AĞRI">AĞRI</option><option value="AKSARAY">AKSARAY</option><option value="AMASYA">AMASYA</option><option value="ANKARA">ANKARA</option><option value="ANTALYA">ANTALYA</option><option value="ARDAHAN">ARDAHAN</option><option value="ARTVİN">ARTVİN</option><option value="AYDIN">AYDIN</option><option value="BALIKESİR">BALIKESİR</option><option value="BARTIN">BARTIN</option><option value="BATMAN">BATMAN</option><option value="BAYBURT">BAYBURT</option><option value="BİLECİK">BİLECİK</option><option value="BİNGÖL">BİNGÖL</option><option value="BİTLİS">BİTLİS</option><option value="BOLU">BOLU</option><option value="BURDUR">BURDUR</option><option value="BURSA">BURSA</option><option value="ÇANAKKALE">ÇANAKKALE</option><option value="ÇANKIRI">ÇANKIRI</option><option value="ÇORUM">ÇORUM</option><option value="DENİZLİ">DENİZLİ</option><option value="DİYARBAKIR">DİYARBAKIR</option><option value="DÜZCE">DÜZCE</option><option value="EDİRNE">EDİRNE</option><option value="ELAZIĞ">ELAZIĞ</option><option value="ERZİNCAN">ERZİNCAN</option><option value="ERZURUM">ERZURUM</option><option value="ESKİŞEHİR">ESKİŞEHİR</option><option value="GAZİANTEP">GAZİANTEP</option><option value="GİRESUN">GİRESUN</option><option value="GÜMÜŞHANE">GÜMÜŞHANE</option><option value="HAKKARİ">HAKKARİ</option><option value="HATAY">HATAY</option><option value="İÇEL">İÇEL</option><option value="IĞDIR">IĞDIR</option><option value="ISPARTA">ISPARTA</option><option value="İSTANBUL">İSTANBUL</option><option value="İZMİR">İZMİR</option><option value="KAHRAMANMARAŞ">KAHRAMANMARAŞ</option><option value="KARABÜK">KARABÜK</option><option value="KARAMAN">KARAMAN</option><option value="KARS">KARS</option><option value="KASTAMONU">KASTAMONU</option><option value="KAYSERİ">KAYSERİ</option><option value="KİLİS">KİLİS</option><option value="KIRIKKALE">KIRIKKALE</option><option value="KIRKLARELİ">KIRKLARELİ</option><option value="KIRŞEHİR">KIRŞEHİR</option><option value="KOCAELİ">KOCAELİ</option><option value="KONYA">KONYA</option><option value="KÜTAHYA">KÜTAHYA</option><option value="MALATYA">MALATYA</option><option value="MANİSA">MANİSA</option><option value="MARDİN">MARDİN</option><option value="MUĞLA">MUĞLA</option><option value="MUŞ">MUŞ</option><option value="NEVŞEHİR">NEVŞEHİR</option><option value="NİĞDE">NİĞDE</option><option value="ORDU">ORDU</option><option value="OSMANİYE">OSMANİYE</option><option value="RİZE">RİZE</option><option value="SAKARYA">SAKARYA</option><option value="SAMSUN">SAMSUN</option><option value="ŞANLIURFA">ŞANLIURFA</option><option value="SİİRT">SİİRT</option><option value="SİNOP">SİNOP</option><option value="ŞIRNAK">ŞIRNAK</option><option value="SİVAS">SİVAS</option><option value="TEKİRDAĞ">TEKİRDAĞ</option><option value="TOKAT">TOKAT</option><option value="TRABZON">TRABZON</option><option value="TUNCELİ">TUNCELİ</option><option value="UŞAK">UŞAK</option><option value="VAN">VAN</option><option value="YALOVA">YALOVA</option><option value="YOZGAT">YOZGAT</option><option value="ZONGULDAK">ZONGULDAK</option>                        </select>\n' +
    '</div>\n' +
    '</div>\n' +

    '<div class="form-group row">\n' +
    '<label for="tel" class="col-sm-2 col-form-label">Telefon:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="number" class="form-control" name="tel" id="tel" required>\n' +
    '</div>\n' +
    '</div>\n' +

    '<div class="form-group row">\n' +
    '<label for="mail" class="col-sm-2 col-form-label">Mail :</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="email" class="form-control" name="mail" id="mail" required>\n' +
    '</div>\n' +
    '</div>\n' +

    '<div class="form-group row">\n' +
    '<hr>\n' +
    '</div>\n' +


    '<div class="form-group row">\n' +
    '<div class="col-sm-12 text-center">\n' +
    '<h5>Araç Bilgileri</h5>\n' +
    '</div>\n' +
    '</div>\n' +

    '<div class="form-group row">\n' +
    '    <label for="ruhsatislemturu" class="col-sm-2 col-form-label">İşlem Türü:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '<label>\n' +
    '<input type="radio" name="ruhsatislemturu" value="yenileme" checked="checked">\n' +
    'Yenileme\n' +
    '</label>\n' +
    '<label>\n' +
    '<input type="radio" name="ruhsatislemturu" value="yenikayit">\n' +
    'Yeni Kayıt\n' +
    '</label>\n' +
    '    </div>\n' +
    '  </div>\n' +

    '<div id="yenilemeyenikayit">\n' +
    '<div class="form-group row">\n' +
    '<label for="belgeno" class="col-sm-2 col-form-label">Belge Seri No:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="text" class="form-control" name="belgeno" id="belgeno">\n' +
    '</div>\n' +
    '</div>\n' +
    '</div>\n' +

    '<div class="form-group row">\n' +
    '<label for="marka" class="col-sm-2 col-form-label">Marka:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<select class="form-control" name="marka" id="marka" required>\n' +
    '<option value="0">Seçiniz</option><option value="AIXAM2">AIXAM2</option><option value="AKIA">AKIA</option><option value="ALFA ROMEO">ALFA ROMEO</option><option value="AR-BUS">AR-BUS</option><option value="ASKAM/FARGO/DESOTO">ASKAM/FARGO/DESOTO</option><option value="ASTON MARTIN">ASTON MARTIN</option><option value="ASTRA">ASTRA</option><option value="AUDI">AUDI</option><option value="AVIA">AVIA</option><option value="BENTLEY">BENTLEY</option><option value="BMC">BMC</option><option value="BMW">BMW</option><option value="BREDAMENARIBUS">BREDAMENARIBUS</option><option value="BUGATTI">BUGATTI</option><option value="BUICK">BUICK</option><option value="BUSCLUB">BUSCLUB</option><option value="CADILLAC">CADILLAC</option><option value="CATERHAM">CATERHAM</option><option value="CHANGAN">CHANGAN</option><option value="CHERY">CHERY</option><option value="CHEVROLET">CHEVROLET</option><option value="CHRYSLER">CHRYSLER</option><option value="CITROEN">CITROEN</option><option value="DACIA">DACIA</option><option value="DAEWOO">DAEWOO</option><option value="DAF">DAF</option><option value="DAIHATSU">DAIHATSU</option><option value="DFM">DFM</option><option value="DODGE/USA">DODGE/USA</option><option value="EICHER">EICHER</option><option value="FAW">FAW</option><option value="FERRARI">FERRARI</option><option value="FIAT">FIAT</option><option value="FISKER">FISKER</option><option value="FOLKVAN">FOLKVAN</option><option value="FORD/OTOSAN">FORD/OTOSAN</option><option value="FORD/USA">FORD/USA</option><option value="GAZ">GAZ</option><option value="GEELY">GEELY</option><option value="GMC">GMC</option><option value="GONOW">GONOW</option><option value="GREAT WALL">GREAT WALL</option><option value="GROVE">GROVE</option><option value="GULERYUZ">GULERYUZ</option><option value="HASKAR">HASKAR</option><option value="HINO">HINO</option><option value="HISCAR">HISCAR</option><option value="HONDA">HONDA</option><option value="HUANGHAI">HUANGHAI</option><option value="HUMMER">HUMMER</option><option value="HYMER">HYMER</option><option value="HYUNDAI">HYUNDAI</option><option value="IKCO">IKCO</option><option value="INFINITI">INFINITI</option><option value="IRIZAR">IRIZAR</option><option value="ISOBUS">ISOBUS</option><option value="ISUZU">ISUZU</option><option value="JAC">JAC</option><option value="JAGUAR">JAGUAR</option><option value="KANUNI">KANUNI</option><option value="KARSAN">KARSAN</option><option value="KENWORTH">KENWORTH</option><option value="KIA">KIA</option><option value="KOENIGSEGG">KOENIGSEGG</option><option value="KTM">KTM</option><option value="LADA">LADA</option><option value="LAMBORGHINI">LAMBORGHINI</option><option value="LANCIA">LANCIA</option><option value="LAND ROVER">LAND ROVER</option><option value="LINCOLN">LINCOLN</option><option value="LOTUS">LOTUS</option><option value="MAHINDRA">MAHINDRA</option><option value="MAN">MAN</option><option value="MASERATI">MASERATI</option><option value="MAYBACH">MAYBACH</option><option value="MAZDA">MAZDA</option><option value="MEGA">MEGA</option><option value="MERCEDES">MERCEDES</option><option value="MERCURY">MERCURY</option><option value="MICROCAR">MICROCAR</option><option value="MINI">MINI</option><option value="MITSUBISHI">MITSUBISHI</option><option value="MJT">MJT</option><option value="MOTORSIKLET">MOTORSIKLET</option><option value="NEOPLAN">NEOPLAN</option><option value="NISSAN">NISSAN</option><option value="OPEL">OPEL</option><option value="OTOKAR/MAGIRUS">OTOKAR/MAGIRUS</option><option value="OTOYOL/IVECO/FIAT">OTOYOL/IVECO/FIAT</option><option value="PETERBILT">PETERBILT</option><option value="PEUGEOT">PEUGEOT</option><option value="PGO">PGO</option><option value="PIAGGIO">PIAGGIO</option><option value="PONTIAC">PONTIAC</option><option value="PORSCHE">PORSCHE</option><option value="PROTON">PROTON</option><option value="RANGE ROVER">RANGE ROVER</option><option value="RENAULT">RENAULT</option><option value="RENAULT (OYAK)">RENAULT (OYAK)</option><option value="ROLLS-ROYCE">ROLLS-ROYCE</option><option value="ROVER">ROVER</option><option value="SAAB">SAAB</option><option value="SAMSUNG">SAMSUNG</option><option value="SCAM">SCAM</option><option value="SCANIA">SCANIA</option><option value="SEAT">SEAT</option><option value="SETRA">SETRA</option><option value="SHUANGHUAN">SHUANGHUAN</option><option value="SINOTRUK">SINOTRUK</option><option value="SKODA">SKODA</option><option value="SMART">SMART</option><option value="SSANGYONG">SSANGYONG</option><option value="SUBARU">SUBARU</option><option value="SUZUKI">SUZUKI</option><option value="TATA">TATA</option><option value="TCV">TCV</option><option value="TEMSA">TEMSA</option><option value="TESLA">TESLA</option><option value="TEZELLER">TEZELLER</option><option value="THE LONDON TAXI">THE LONDON TAXI</option><option value="TOFAS-FIAT">TOFAS-FIAT</option><option value="TOYOTA">TOYOTA</option><option value="TURKAR">TURKAR</option><option value="TURKKAR">TURKKAR</option><option value="VANHOLL">VANHOLL</option><option value="VEICOLI">VEICOLI</option><option value="VISEON">VISEON</option><option value="VOLKSWAGEN">VOLKSWAGEN</option><option value="VOLVO">VOLVO</option><option value="VOLVO-TR">VOLVO-TR</option><option value="WIESMANN">WIESMANN</option><option value="ZIRAI TRAKTOR">ZIRAI TRAKTOR</option><option value="ZONDA">ZONDA</option>\n' +
    '</select>\n' +
    '</div>\n' +
    '</div>\n' +

    '<div class="form-group row">\n' +
    '<label for="model" class="col-sm-2 col-form-label">Model:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="text" class="form-control" name="model" id="model" required>\n' +
    '</div>\n' +
    '</div>\n' +

    '<div class="form-group row">\n' +
    '<label for="modelyili" class="col-sm-2 col-form-label">Model Yılı:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="text" class="form-control" name="modelyili" id="modelyili" required>\n' +
    '</div>\n' +
    '</div>\n' +

    '<div class="form-group row">\n' +
    '<label for="plaka" class="col-sm-2 col-form-label">Plaka:</label>\n' +
    '<div class="col-sm-10">\n' +
    '<input type="text" class="form-control" name="plaka" id="plaka" required>\n' +
    '</div>\n' +
    '</div>\n' +

    '<div class="form-group row">\n' +
    '<div class="col-sm-12 text-center">\n' +
    '<button type="submit" class="in-button">TEKLİF AL</button>\n' +
    '</div>\n' +
    '</div>\n' +

    '</form>\n' +
    '</div>',
    saglik:'<div class="container">\n' +
    '    <form id="FormTeklifAl">\n' +
    '    <div class="form-group row">\n' +
    '    <label for="tc" class="col-sm-2 col-form-label">T.C. Kimlik No:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <input type="number" class="form-control" name="tc" id="tc" required>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '    <div class="form-group row">\n' +
    '    <label for="adsoyad" class="col-sm-2 col-form-label">Adınız Soyadınız:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <input type="text" class="form-control" name="adsoyad" id="adsoyad" required>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\t<div class="form-group row">\n' +
    '    <label for="dTarih" class="col-sm-2 col-form-label">Doğum Tarihiniz:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <input type="text" class="form-control" name="dTarih" id="dTarih" required>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\t<div class="form-group row">\n' +
    '    <label for="meslek" class="col-sm-2 col-form-label">Mesleğiniz:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <input type="text" class="form-control" name="meslek" id="meslek" required>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '    <div class="form-group row">\n' +
    '    <label for="il" class="col-sm-2 col-form-label">Yaşadığınız Şehir:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <select class="form-control" name="il" id="il" required>\n' +
    '    <option value="0">Seçiniz</option>\n' +
    '    <option value="ADANA">ADANA</option><option value="ADIYAMAN">ADIYAMAN</option><option value="AFYON">AFYON</option><option value="AĞRI">AĞRI</option><option value="AKSARAY">AKSARAY</option><option value="AMASYA">AMASYA</option><option value="ANKARA">ANKARA</option><option value="ANTALYA">ANTALYA</option><option value="ARDAHAN">ARDAHAN</option><option value="ARTVİN">ARTVİN</option><option value="AYDIN">AYDIN</option><option value="BALIKESİR">BALIKESİR</option><option value="BARTIN">BARTIN</option><option value="BATMAN">BATMAN</option><option value="BAYBURT">BAYBURT</option><option value="BİLECİK">BİLECİK</option><option value="BİNGÖL">BİNGÖL</option><option value="BİTLİS">BİTLİS</option><option value="BOLU">BOLU</option><option value="BURDUR">BURDUR</option><option value="BURSA">BURSA</option><option value="ÇANAKKALE">ÇANAKKALE</option><option value="ÇANKIRI">ÇANKIRI</option><option value="ÇORUM">ÇORUM</option><option value="DENİZLİ">DENİZLİ</option><option value="DİYARBAKIR">DİYARBAKIR</option><option value="DÜZCE">DÜZCE</option><option value="EDİRNE">EDİRNE</option><option value="ELAZIĞ">ELAZIĞ</option><option value="ERZİNCAN">ERZİNCAN</option><option value="ERZURUM">ERZURUM</option><option value="ESKİŞEHİR">ESKİŞEHİR</option><option value="GAZİANTEP">GAZİANTEP</option><option value="GİRESUN">GİRESUN</option><option value="GÜMÜŞHANE">GÜMÜŞHANE</option><option value="HAKKARİ">HAKKARİ</option><option value="HATAY">HATAY</option><option value="İÇEL">İÇEL</option><option value="IĞDIR">IĞDIR</option><option value="ISPARTA">ISPARTA</option><option value="İSTANBUL">İSTANBUL</option><option value="İZMİR">İZMİR</option><option value="KAHRAMANMARAŞ">KAHRAMANMARAŞ</option><option value="KARABÜK">KARABÜK</option><option value="KARAMAN">KARAMAN</option><option value="KARS">KARS</option><option value="KASTAMONU">KASTAMONU</option><option value="KAYSERİ">KAYSERİ</option><option value="KİLİS">KİLİS</option><option value="KIRIKKALE">KIRIKKALE</option><option value="KIRKLARELİ">KIRKLARELİ</option><option value="KIRŞEHİR">KIRŞEHİR</option><option value="KOCAELİ">KOCAELİ</option><option value="KONYA">KONYA</option><option value="KÜTAHYA">KÜTAHYA</option><option value="MALATYA">MALATYA</option><option value="MANİSA">MANİSA</option><option value="MARDİN">MARDİN</option><option value="MUĞLA">MUĞLA</option><option value="MUŞ">MUŞ</option><option value="NEVŞEHİR">NEVŞEHİR</option><option value="NİĞDE">NİĞDE</option><option value="ORDU">ORDU</option><option value="OSMANİYE">OSMANİYE</option><option value="RİZE">RİZE</option><option value="SAKARYA">SAKARYA</option><option value="SAMSUN">SAMSUN</option><option value="ŞANLIURFA">ŞANLIURFA</option><option value="SİİRT">SİİRT</option><option value="SİNOP">SİNOP</option><option value="ŞIRNAK">ŞIRNAK</option><option value="SİVAS">SİVAS</option><option value="TEKİRDAĞ">TEKİRDAĞ</option><option value="TOKAT">TOKAT</option><option value="TRABZON">TRABZON</option><option value="TUNCELİ">TUNCELİ</option><option value="UŞAK">UŞAK</option><option value="VAN">VAN</option><option value="YALOVA">YALOVA</option><option value="YOZGAT">YOZGAT</option><option value="ZONGULDAK">ZONGULDAK</option>                        </select>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="form-group row">\n' +
    '    <label for="tel" class="col-sm-2 col-form-label">Telefon:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <input type="number" class="form-control" name="tel" id="tel" required>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="form-group row">\n' +
    '    <label for="mail" class="col-sm-2 col-form-label">Mail:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <input type="email" class="form-control" name="mail" id="mail" required>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="form-group row">\n' +
    '    <label for="teminaticerigi" class="col-sm-2 col-form-label">Teminat İçeriği:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <label>\n' +
    '    <input type="radio" name="teminaticerigi" value="Sadece Yatarak Tedavi" checked="checked">\n' +
    '    Sadece Yatarak Tedavi\n' +
    '    </label><br>\n' +
    '    <label>\n' +
    '    <input type="radio" name="teminaticerigi" value="Yatarak + Ayakta Tedavi">\n' +
    '    Yatarak + Ayakta Tedavi\n' +
    '    </label><br>\n' +
    '\t<label>\n' +
    '    <input type="radio" name="teminaticerigi" value="Yatarak + Ayakta + Doğum Teminatı">\n' +
    '    Yatarak + Ayakta + Doğum Teminatı\n' +
    '    </label>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\n' +
    '\t<div class="form-group row">\n' +
    '    <label for="" class="col-sm-2 col-form-label">Not:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <p><b>Yatarak Tedavi Teminatı:</b> 1 gün veya daha fazla süre hastane yatışı gerektiren hastalıklar ile kırık, yanık, zehirlenme, arı sokması, 37 derece ateş vb. küçük müdahaleler.</p>\n' +
    '\t<p><b>Ayakta Tedavi Teminatı:</b>\tDoktor Muayenesi, ilaç, tahlil, röntgen ve ileri tanı yöntemleri</p>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="form-group row">\n' +
    '    <div class="col-sm-12 text-center">\n' +
    '    <button type="submit" class="in-button">TEKLİF AL</button>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\n' +
    '    </form>\n' +
    '    </div>',

    konut:'<div class="container">\n' +
    '    <form id="FormTeklifAl">\n' +
    '    <div class="form-group row">\n' +
    '    <div class="col-sm-12 text-center">\n' +
    '    <h5>Konut Sahibi Bilgileri</h5>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '    <div class="form-group row">\n' +
    '    <label for="adsoyad" class="col-sm-2 col-form-label">Adınız Soyadınız:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <input type="text" class="form-control" name="adsoyad" id="adsoyad" required>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\t\n' +
    '\t<div class="form-group row">\n' +
    '    <label for="tel" class="col-sm-2 col-form-label">Telefon:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <input type="number" class="form-control" name="tel" id="tel" required>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="form-group row">\n' +
    '    <label for="mail" class="col-sm-2 col-form-label">Mail:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <input type="email" class="form-control" name="mail" id="mail" required>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\t\n' +
    '\t<div class="form-group row">\n' +
    '    <div class="col-sm-12 text-center">\n' +
    '    <h5>Konut Bilgileri</h5>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\t\n' +
    '    <div class="form-group row">\n' +
    '    <label for="il" class="col-sm-2 col-form-label">Şehir:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <select class="form-control" name="il" id="il" required>\n' +
    '    <option value="0">Seçiniz</option>\n' +
    '    <option value="ADANA">ADANA</option><option value="ADIYAMAN">ADIYAMAN</option><option value="AFYON">AFYON</option><option value="AĞRI">AĞRI</option><option value="AKSARAY">AKSARAY</option><option value="AMASYA">AMASYA</option><option value="ANKARA">ANKARA</option><option value="ANTALYA">ANTALYA</option><option value="ARDAHAN">ARDAHAN</option><option value="ARTVİN">ARTVİN</option><option value="AYDIN">AYDIN</option><option value="BALIKESİR">BALIKESİR</option><option value="BARTIN">BARTIN</option><option value="BATMAN">BATMAN</option><option value="BAYBURT">BAYBURT</option><option value="BİLECİK">BİLECİK</option><option value="BİNGÖL">BİNGÖL</option><option value="BİTLİS">BİTLİS</option><option value="BOLU">BOLU</option><option value="BURDUR">BURDUR</option><option value="BURSA">BURSA</option><option value="ÇANAKKALE">ÇANAKKALE</option><option value="ÇANKIRI">ÇANKIRI</option><option value="ÇORUM">ÇORUM</option><option value="DENİZLİ">DENİZLİ</option><option value="DİYARBAKIR">DİYARBAKIR</option><option value="DÜZCE">DÜZCE</option><option value="EDİRNE">EDİRNE</option><option value="ELAZIĞ">ELAZIĞ</option><option value="ERZİNCAN">ERZİNCAN</option><option value="ERZURUM">ERZURUM</option><option value="ESKİŞEHİR">ESKİŞEHİR</option><option value="GAZİANTEP">GAZİANTEP</option><option value="GİRESUN">GİRESUN</option><option value="GÜMÜŞHANE">GÜMÜŞHANE</option><option value="HAKKARİ">HAKKARİ</option><option value="HATAY">HATAY</option><option value="İÇEL">İÇEL</option><option value="IĞDIR">IĞDIR</option><option value="ISPARTA">ISPARTA</option><option value="İSTANBUL">İSTANBUL</option><option value="İZMİR">İZMİR</option><option value="KAHRAMANMARAŞ">KAHRAMANMARAŞ</option><option value="KARABÜK">KARABÜK</option><option value="KARAMAN">KARAMAN</option><option value="KARS">KARS</option><option value="KASTAMONU">KASTAMONU</option><option value="KAYSERİ">KAYSERİ</option><option value="KİLİS">KİLİS</option><option value="KIRIKKALE">KIRIKKALE</option><option value="KIRKLARELİ">KIRKLARELİ</option><option value="KIRŞEHİR">KIRŞEHİR</option><option value="KOCAELİ">KOCAELİ</option><option value="KONYA">KONYA</option><option value="KÜTAHYA">KÜTAHYA</option><option value="MALATYA">MALATYA</option><option value="MANİSA">MANİSA</option><option value="MARDİN">MARDİN</option><option value="MUĞLA">MUĞLA</option><option value="MUŞ">MUŞ</option><option value="NEVŞEHİR">NEVŞEHİR</option><option value="NİĞDE">NİĞDE</option><option value="ORDU">ORDU</option><option value="OSMANİYE">OSMANİYE</option><option value="RİZE">RİZE</option><option value="SAKARYA">SAKARYA</option><option value="SAMSUN">SAMSUN</option><option value="ŞANLIURFA">ŞANLIURFA</option><option value="SİİRT">SİİRT</option><option value="SİNOP">SİNOP</option><option value="ŞIRNAK">ŞIRNAK</option><option value="SİVAS">SİVAS</option><option value="TEKİRDAĞ">TEKİRDAĞ</option><option value="TOKAT">TOKAT</option><option value="TRABZON">TRABZON</option><option value="TUNCELİ">TUNCELİ</option><option value="UŞAK">UŞAK</option><option value="VAN">VAN</option><option value="YALOVA">YALOVA</option><option value="YOZGAT">YOZGAT</option><option value="ZONGULDAK">ZONGULDAK</option>                        </select>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\t\n' +
    '\t<div class="form-group row">\n' +
    '    <label for="mkare" class="col-sm-2 col-form-label">Metre Kare:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <input type="text" class="form-control" name="mkare" id="mkare" required>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\t\n' +
    '\t<div class="form-group row">\n' +
    '    <label for="konut_bedeli" class="col-sm-2 col-form-label">Konut Bedeli:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <input type="number" class="form-control" name="konut_bedeli" id="konut_bedeli" required>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\t\n' +
    '\t<div class="form-group row">\n' +
    '    <label for="adres" class="col-sm-2 col-form-label">Açık Adres:</label>\n' +
    '    <div class="col-sm-10">\n' +
    '    <textarea class="form-control" name="adres" id="adres" required></textarea>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="form-group row">\n' +
    '    <div class="col-sm-12 text-center">\n' +
    '    <button type="submit" class="in-button">TEKLİF AL</button>\n' +
    '    </div>\n' +
    '    </div>\n' +
    '\n' +
    '    </form>\n' +
    '    </div>'
};


function FuncSigortaTeklifi(turu, baslik) {
    jcTeklif = jQuery.dialog({
        columnClass: 'col-md-12',
        theme:'modern',
        type:'red',
        title:baslik,
        content:sigortateklifi[turu],
        onOpenBefore:function () {
            $('form#FormTeklifAl').append('<input type="hidden" name="teklifturu" value="'+turu+'"/>');
            $('form#FormTeklifAl input').prop('autocomplete','off');
            $('form#FormTeklifAl input[type="radio"]').iCheck({
                radioClass:'iradio_square-red'
            });

            $('form#FormTeklifAl input[type="radio"]').on('ifClicked', function(event){
                if($(this).val() == 'sirket'){
                    $('form#FormTeklifAl #sahissirket').html(sirket);
                    // $('form#FormTeklifAl div#sirket').show();
                    // $('form#FormTeklifAl div#sahis').hide();
                }else if($(this).val() == 'sahis'){
                    // $('form#FormTeklifAl div#sahis').show();
                    // $('form#FormTeklifAl div#sirket').hide();
                    $('form#FormTeklifAl #sahissirket').html(sahis);
                }else if($(this).val() == 'yenileme'){
                    $('form#FormTeklifAl #yenilemeyenikayit').html(yenileme);
                    // $('form#FormTeklifAl div#yenileme').show();
                    // $('form#FormTeklifAl div#yenikayit').hide();
                }else if($(this).val() == 'yenikayit'){
                    $('form#FormTeklifAl #yenilemeyenikayit').html(yenikayit);
                    // $('form#FormTeklifAl div#yenikayit').show();
                    // $('form#FormTeklifAl div#yenileme').hide();
                }
            });
        }

    });
}

</script>
@endsection