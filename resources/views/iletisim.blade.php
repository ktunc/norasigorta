@extends('app')

@section('content')
    <!-- Contact Area -->
    <div class="contact-area in-section section-padding-xs">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        {{--<h6>CONTACT US AT ANY TIME</h6>--}}
                        <h2>İLETİŞİM</h2>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-12">
                    <!-- contact-form-warp Start -->
                    <div class="contact-form-warp">
                        <form id="contact-form" action="http://demo.hasthemes.com/gregory-v1/mail.php" method="post">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="input-box">
                                        <input name="name" placeholder="İsim *" type="text">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="input-box">
                                        <input name="name" placeholder="Soyisim *" type="text">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="input-box">
                                        <input name="email" placeholder="Email *" type="email">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="input-box">
                                        <input name="phone" placeholder="Telefon *" type="text">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="input-box">
                                        <textarea name="message" placeholder="Mesaj *"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="contact-submit-btn text-center">
                                <button type="submit" class="submit-btn default-btn">Gönder</button>
                                <p class="form-messege"></p>
                            </div>
                        </form>
                    </div>
                    <!-- contact-form-warp End -->
                </div>

            </div>
            <div class="row">
                <div class="col">
                    <div class="map-area section-padding-top-lg">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// Services Area -->


    <!-- google map js -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAq7MrCR1A2qIShmjbtLHSKjcEIEBEEwM"></script>
    <script>
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 12,

                scrollwheel: false,

                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(23.761226, 90.40766), // New York

                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#01416e"
                    }]
                },
                    {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [{
                            "color": "#13253c"
                        }]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "road",
                        "elementType": "all",
                        "stylers": [{
                            "saturation": -100
                        },
                            {
                                "lightness": 45
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "simplified"
                        }]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.icon",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [{
                            "color": "#314453"
                        },
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry.fill",
                        "stylers": [{
                            "lightness": "-12"
                        },
                            {
                                "saturation": "0"
                            },
                            {
                                "color": "#0e3352"
                            }
                        ]
                    }
                ]
            };

            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(23.761226, 90.420766),
                map: map,
                title: 'Snazzy!'
            });
        }
    </script>
@endsection