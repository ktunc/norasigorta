<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Norasigorta') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/admin/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-confirm.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/iCheck/all.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/admin/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/admin/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/admin/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('js/admin/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('js/admin/inspinia.js') }}"></script>
    <script src="{{ asset('js/admin/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('js/jquery-confirm.min.js') }}"></script>
    <script src="{{ asset('js/blockUI.js') }}"></script>
    <script src="{{ asset('js/iCheck/iCheck.min.js') }}"></script>

    {{--<script src="{{ asset('js/admin/plugins/jquery-ui/jquery-ui.min.js') }}"></script>--}}

    <!-- Fonts -->
    {{--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,400,400i,600,700" rel="stylesheet">--}}


</head>
<body class="">

<div id="wrapper">

{{--@include('header')--}}

<!-- Page Conttent -->
    <main class="page-content">
        @auth
            @include('admin.leftmenu')
            <div id="page-wrapper" class="gray-bg dashbard-1">
                @include('admin.header')
                @yield('content')
            </div>
        @endauth
        @guest
            @yield('content')
        @endguest
    </main>

    {{--@include('footer')--}}
</div>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
</body>
</html>
