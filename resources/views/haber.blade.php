@extends('app')

@section('content')
    <link href="{{ asset('css/admin/plugins/blueimp/css/blueimp-gallery.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/admin/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>

    <!-- About Area -->
    <div class="about-area in-section section-padding-top-xxs bg-white">
        <div class="container custom-container">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="about-content heightmatch">
                        <h4>{{$haber->baslik}}</h4>

                        <?=$haber->icerik?>

                        <div class="row lightBoxGallery">
                            @foreach($haber->haberresimleri as $row)
                                <div class="col-xs-12 col-sm-3">
                                    <a href="{{asset('storage/'.$row->path)}}" data-gallery=""><img src="{{asset('storage/'.$row->path)}}" alt="{{$haber->baslik.'-'.$row->id}}" style="width:300px;height:200px;" /></a>
                                </div>
                            @endforeach
                                <div id="blueimp-gallery" class="blueimp-gallery">
                                    <div class="slides"></div>
                                    <h3 class="title"></h3>
                                    <a class="prev">‹</a>
                                    <a class="next">›</a>
                                    <a class="close">×</a>
                                    <a class="play-pause"></a>
                                    <ol class="indicator"></ol>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// About Area -->
@endsection