<!-- Header -->
<header class="header" id="header">

    <!-- Header Top Area -->
    <div class="header-toparea">
        <div class="container">
            <div class="row justify-content-betwween">
                <div class="col-lg-12">
                    <ul class="header-topcontact">
                        <li><i class="zmdi zmdi-phone"></i> Telefon : <a href="#">+90(544)9708102</a></li>
                        <li><i class="zmdi zmdi-email"></i> E-Mail : <a href="#">leventmurat@norasigorta.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--// Header Top Area -->

    <!-- Header Bottom Area -->
    <div id="header-bottomarea">
    <div class="header-bottomarea">
        <div class="container">
            <div class="header-bottom">

                <!-- Header Logo -->
                <a href="{{url('/')}}" class="header-logo">
                    <img src="{{ asset('images/logo/logo.png') }}" class="img-logo" alt="header image">
                </a>
                <!--// Header Logo -->

                <!-- Main Navigation -->
                <nav class="in-navigation">
                    <ul>
                        <li><a href="{{url('/')}}">ANASAYFA</a></li>
                        <li class="in-dropdown"><a href="services.html">KURUMSAL</a>
                            <ul>
                                <li><a href="{{url('hakkimizda')}}">HAKKIMIZDA</a></li>
                                <li><a href="{{url('misyonvizyon')}}">MİSYON & VİZYON</a></li>
                                <li><a href="{{url('degerlerimiz')}}">DEĞERLERİMİZ</a></li>
                            </ul>
                        </li>
                        <li class="in-dropdown"><a href="#">ÜRÜNLER</a>
                            <ul style="max-height: 400px; overflow-y: auto;">
                                <li><a href="{{url('/bireysel_emeklilik')}}">Bireysel Emeklilik</a></li>
                                <li><a href="{{url('/ferdi_kaza')}}">Ferdi Kaza Sigortası</a></li>
                                <li><a href="{{url('/isyeri')}}">İş Yeri Sigortaları</a></li>
                                <li><a href="{{url('/hekim_sorumluluk')}}">Hekim Sorumluluk</a></li>
                                <li><a href="{{url('/kasko')}}">Kasko</a></li>
                                <li><a href="{{url('/muhendislik')}}">Mühendislik Sigortaları</a></li>
                                <li><a href="{{url('/nakliyat')}}">Nakliyat Sigortaları</a></li>
                                <li><a href="{{url('/saglik')}}">Sağlık Sigortaları</a></li>
                                <li><a href="{{url('/seyahat')}}">Seyahat Sigortaları</a></li>
                                <li><a href="{{url('/sorumluluk')}}">Sorumluluk Sigortaları</a></li>
                                <li><a href="{{url('/tarim')}}">Tarım Sigortaları</a></li>
                                <li><a href="{{url('/trafik')}}">Trafik Sigortası</a></li>
                                <li><a href="{{url('/yangin')}}">Yangın Sigortası</a></li>
                                <li><a href="{{url('/yat_tekne')}}">Yat-Tekne Sigortaları</a></li>
                                <li><a href="{{url('/konut')}}">Konut Sigortaları</a></li>
                                <li><a href="{{url('/hayat')}}">Hayat Sigortası</a></li>
                            </ul>
                        </li>
                        <li class="in-dropdown"><a href="#">HİZMETLERİMİZ</a>
                            <ul>
                                <li><a href="hizmet_hasar">Hasar Anında</a></li>
                                <li><a href="hizmet_arac_deger">Araç Değer Kaybı</a></li>
                                <li><a href="hizmet_kurumsal_risk">Kurumsal Risk Yönetimi</a></li>
                                <li><a href="hizmet_sigorta_danismanlik">Sigorta Danışmanlık Hizmetleri</a></li>
                                <li><a href="hizmet_ticari_alacak">Ticari Alacak Sigortaları</a></li>
                            </ul>
                        </li>
                        <li><a href="iletisim">İLETİŞİM</a></li>
                    </ul>
                </nav>
                <!--// Main Navigation -->

                {{--<div class="header-right-wrap">--}}
                    {{--<!-- Header Search -->--}}
                    {{--<div class="header-search">--}}
                        {{--<button class="header-searchtrigger"><i class="zmdi zmdi-search"></i></button>--}}
                        {{--<form class="header-searchbox" action="#">--}}
                            {{--<input type="text" placeholder="Search...">--}}
                            {{--<button type="submit"><i class="zmdi zmdi-search"></i></button>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                    {{--<!--// Header Search -->--}}

                    {{--<!-- Buy Now -->--}}
                    {{--<div class="buy-now">--}}
                        {{--<a href="http://1.envato.market/rE46d" class="buy-now-button">Buy Now</a>--}}
                    {{--</div>--}}
                    {{--<!--// Buy Now -->--}}

                {{--</div>--}}

            </div>
        </div>
    </div>
    <!--// Header Bottom Area -->
    <!-- Mobile Menu -->
    <div class="mobile-menu-wrapper clearfix">
        <div class="container">
            <div class="mobile-menu"></div>
        </div>
    </div>
    </div>
    <!--// Mobile Menu -->

</header>
<!--// Header -->